require 'fancy_irb'
require "awesome_print"
require "paint"
require "bond"
require "looksee"

require "hirb"
require "wirb"

require 'paint/pa'
Bond.start
AwesomePrint.irb!
Wirb.start
FancyIrb.start


# directly write the last statement to the currently openned file in vim
def wc
  statement = IRB.CurrentContext.io.line(-2)
  vim_cmd = "<esc>o#{statement.strip}<esc>"
  cmd = "vim --servername loper --remote-send '#{vim_cmd}'"
  system cmd
end

puts "Loaded loper irbrc"

