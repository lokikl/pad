#!/usr/bin/env ruby

def adb(cmd)
  @device ||= `adb devices | grep -v 'emulator' | tail -n2 | head -n1 | awk '{print $1}'`.strip
  `adb -s #{@device} #{cmd}`
end

def tap(x, y)
  interval = 8000
  split = 15
  ss = (177.0 / split).ceil

  time = 0
  steps = [[x, y]]
  steps << steps.last

  sl = steps.length
  data = []
  steps.each { |nx, ny|
    dx = nx > x ? ss : nx == x ? 0 : -ss
    dy = ny > y ? ss : ny == y ? 0 : -ss
    split.times { |i|
      time += interval
      time = interval if time > 99999
      if i == split - 1
        x = nx
        y = ny
      else
        x += dx
        y += dy
      end
      data << [time, 3, 55, 0]
      data << [time, 3, 54, y]
      data << [time, 0, 2, 0]
      time += interval
      data << [time, 3, 57, 0]
      data << [time, 3, 53, x]
      data << [time, 3, 58, 62]
      data << [time, 0, 0, 0]
    }
  }
  time += interval
  3.times { |i|
    data << [time, 0, 2, 0]
    data << [time, 0, 0, 0]
  }
  File.open("tmp_event", 'wb' ) do |output|
    output.write(data.flatten.pack("qSSl" * data.length))
  end
  adb("push ./tmp_event /sdcard/tmp_event")
  adb("shell 'sh /sdcard/dusk-run-real.sh > /dev/input/event5'")
end

def tap_and_wait(msg, x, y, secs=1)
  puts msg
  tap(x, y)
  print "wait for #{secs} secs: "
  secs.times { |i|
    print(i + 1)
    sleep(1)
  }
  puts "\n\n"
end

# detected by shell function: adb-get-pos
def play_a_game(round)
  tap_and_wait("r#{round} select stage", 824, 1185) # stage appear at 2nd last
  # tap_and_wait("r#{round} select stage", 824, 1305) # stage appear at last

  tap_and_wait("r#{round} select level", 567, 1366)

  tap_and_wait("r#{round} select frd", 538, 650) # frd appear at first
  # tap_and_wait("r#{round} select frd", 538, 770) # frd appear at 2nd

  tap_and_wait("r#{round} select team", 595, 1064)
  tap_and_wait("r#{round} confirm", 585, 1358, 9)
  tap_and_wait("r#{round} 2nd skill", 274, 660, 7)
  tap_and_wait("r#{round} 1st skill", 124, 660, 7)
  tap_and_wait("r#{round} 5th skill", 817, 660, 7)
  tap_and_wait("r#{round} 6th skill", 981, 660, 7)
  tap_and_wait("r#{round} 4th skill", 645, 660, 9)
  tap_and_wait("r#{round} confirm won", 580, 1571, 14)
  tap_and_wait("r#{round} finish", 580, 1514, 2)
  tap_and_wait("r#{round} calculate frd pts", 559, 1191, 2)
end

17.times { |i|
  play_a_game(i + 1)
}
