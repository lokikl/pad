f=/sdcard/tmp_event
e=/sdcard/e
bytes="$(cat $f | wc -c)"
batch=$((16 * 7 * 3))
count=$((bytes / $batch + 1))

for i in 0 `seq $count`; do
  dd if=$f bs=$batch count=1 skip=$i 2>/dev/null
done
