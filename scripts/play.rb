#!/usr/bin/env ruby

=begin to capture
dd if=/dev/input/event5 of=/sdcard/event
adb pull /sdcard/event ./
bundle exec ./play.rb
=end


e = File.read("event")
data = e.unpack("qSSl" * (e.size / 16)).each_slice(4).to_a

data.delete_if { |d| d[1] == 17 || d[1] > 100 }
require 'pry'; binding.pry

write_and_push = ->(data) {
  # write back
  File.open("tmp_event", 'wb' ) do |output|
    output.write data.flatten.pack("qSSl" * data.length)
  end
  `adb push ./tmp_event /sdcard/tmp_event`
  `adb shell "cat /sdcard/tmp_event >> /dev/input/event5"`
}
