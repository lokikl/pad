{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.7 Exporter",
		"vertices"      : 8,
		"faces"         : 2,
		"normals"       : 2,
		"colors"        : 0,
		"uvs"           : [4],
		"materials"     : 2,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "front",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorDiffuse" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorEmissive" : [0.0, 0.0, 0.0],
		"colorSpecular" : [0.5, 0.5, 0.5],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "business-card-front.png",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	},

	{
		"DbgColor" : 15597568,
		"DbgIndex" : 1,
		"DbgName" : "back",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorDiffuse" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorEmissive" : [0.0, 0.0, 0.0],
		"colorSpecular" : [0.5, 0.5, 0.5],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "business-card-back.png",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],

	"vertices" : [-9,2.36042e-07,5.4,9,2.36042e-07,5.4,-9,-2.36042e-07,-5.4,9,-2.36042e-07,-5.4,-9,2.36042e-07,5.4,9,2.36042e-07,5.4,-9,-2.36042e-07,-5.4,9,-2.36042e-07,-5.4],

	"morphTargets" : [],

	"normals" : [0,1,0,0,-1,0],

	"colors" : [],

	"uvs" : [[0.0001,0.0001,0.9999,0.0001,0.9999,0.9999,0.0001,0.9999]],

	"faces" : [43,0,1,3,2,1,0,1,2,3,0,0,0,0,43,4,6,7,5,0,3,0,1,2,1,1,1,1],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

  "animations" : []


}
