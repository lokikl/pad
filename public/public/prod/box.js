{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.7 Exporter",
		"vertices"      : 8,
		"faces"         : 6,
		"normals"       : 8,
		"colors"        : 0,
		"uvs"           : [12],
		"materials"     : 2,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "content-outside",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorDiffuse" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorEmissive" : [0.0, 0.0, 0.0],
		"colorSpecular" : [0.0, 0.0, 0.0],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "content-outside.png",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	},

	{
		"DbgColor" : 15597568,
		"DbgIndex" : 1,
		"DbgName" : "content-inside",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorDiffuse" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorEmissive" : [0.0, 0.0, 0.0],
		"colorSpecular" : [0.0, 0.0, 0.0],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "content-inside.png",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],

	"vertices" : [1,-1,-1,1,-1,1,-1,-1,1,-1,-1,-1,1,1,-1,0.999999,1,1,-1,1,1,-1,1,-1],

	"morphTargets" : [],

	"normals" : [0.577349,-0.577349,-0.577349,0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,-0.577349,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,0.577349,0.577349,0.577349,0.577349],

	"colors" : [],

	"uvs" : [[0.666667,0,1,0,1,0.5,0.666667,0.5,0,1,0,0.5,0.333333,0.5,0.333333,1,0,0,0.333333,0,1,1,0.666667,1]],

	"faces" : [43,0,1,2,3,0,0,1,2,3,0,1,2,3,43,4,7,6,5,0,4,5,6,7,4,5,6,7,43,0,4,5,1,1,5,8,9,6,0,4,7,1,43,1,5,6,2,1,6,9,0,3,1,7,6,2,43,2,6,7,3,1,3,2,10,11,2,6,5,3,43,4,0,3,7,1,6,3,11,7,4,0,3,5],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

  "animations" : []


}
