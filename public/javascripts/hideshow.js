$(function() {

  function hide(query) {
    $(query)
      .addClass('hidden')
      .find('.disable-when-hidden:hidden').attr('disabled', 'disabled');
  }

  function show(query, interval) {
    if (!query) return;
    if (interval === null) {
      interval = 600;
    }
    var $ele = $(query);
    if ($ele.hasClass('hidden')) {
      $ele
        .removeClass('hidden')
        .css('opacity', 0)
        .animate({'opacity': 1}, interval);
    }
    $ele.find('.disable-when-hidden:visible').removeAttr('disabled');
  }

  $('body').on('click.hideshow', '[data-hide]', function() {
    var $this = $(this);
    var q = $this.data();

    if ($this.hasClass('trigger-scrollreveal')) {
      setTimeout(function() {
        window.reveal.init();
      }, q.interval + 100);
    }

    hide(q.hide);
    show(q.show, q.interval);

    $this.parents('.hideshow-parent')
      .find('.hideshow-current').removeClass('hideshow-current');

    $this.addClass('hideshow-current');
  });

  $('body').on('click.hideshow', '[data-toggle]', function() {
    var q = $(this).data('toggle');
    $(q).each(function() {
      var $ele = $(this);
      if ($ele.hasClass('hidden')) {
        show($ele);
      } else {
        hide($ele);
      }
    });
  });

  $('body').on('change.hideshow', 'select[data-hideshow]', function() {
    hide($(this).data('hideshow'));
    $(this).children('option:selected').each(function() {
      show($(this).data('show'));
    });
  });

  $(window).on('statechangecomplete', function() {
    $('.hideshow-current').each(function() {
      var $this = $(this);
      if ($this.is('option')) {
        $this.parent().trigger('change.hideshow');
      } else {
        $this.trigger('click.hideshow');
      }
    });
  });

  $('body').on('click', '[data-pick-one]', function() {
    var $this = $(this);
    $($this.data('pick-one')).prop('checked', false);
    $this.prop('checked', true);
  });

});
