"needs xdotool and wmctrl
"clone https://github.com/jordansissel/xdotool
"make && sudo make install

let loper_stage = 'dusk'
nnoremap sa :exec 'CtrlP app/controllers/' . loper_stage<cr>
nnoremap sc :exec 'CtrlP app/css/' . loper_stage<cr>
nnoremap sh :exec 'CtrlP app/views/' . loper_stage<cr>
nnoremap sj :exec 'CtrlP app/js/' . loper_stage<cr>
nnoremap sm :exec 'CtrlP app/models/' . loper_stage<cr>
nnoremap sl :exec 'CtrlP app/locales/' . loper_stage<cr>
nnoremap sd :exec 'CtrlP db/schema/' . loper_stage<cr>
nnoremap sy :exec 'CtrlP config/deploy/config'<cr>

" new approach: restart loper on a tmux session, find loper window
nnoremap <leader>r :call RestartLoper()<cr>
nnoremap <leader>rs :call RestartShopadmin()<cr>

" Restart loper app
function! RestartLoper()
  if ! exists("g:loper_stage")
    echo "Stage not set"
  else
    let p = getcwd()
    let cmd = " C-c ' cd " . p . " ; STAGE=" . g:loper_stage . " ru 9999' Enter"
    call TmuxSendKey("main", "loper", cmd)
  endif
endfunction

" Restart shopadmin app
function! RestartShopadmin()
  let p = getcwd()
  let cmd = " C-c ' cd " . p . " ; STAGE=shopadmin ru 9998' Enter"
  call TmuxSendKey("main", "shopadmin", cmd)
endfunction
" ============================================================================

autocmd BufEnter *.js nnoremap rjs :RequreJS 
command! -complete=custom,JSLibs -nargs=1 RequreJS exec "normal i//= require vendor/<args>"
function! JSLibs(A,L,P)
  return system("ls -1 app/js/vendor | sed -e 's/\.js//'")
endfunction

autocmd BufEnter *.scss nnoremap rcss :RequreCSS 
command! -complete=custom,CSSLibs -nargs=1 RequreCSS exec "normal i//= require vendor/<args>"
function! CSSLibs(A,L,P)
  return system("ls -1 app/css/vendor | sed -e 's/\.css//'")
endfunction

function! Rspec_current()
  let path = expand("%:p")
  exe "silent ! tmux send-keys -t rspec " . path . " Enter &"
endfunction
nnoremap <F2> :call Rspec_current()<cr>

nnoremap <leader>i :exe "normal vit\ti18l\t"<cr>

if !has("ruby")
  echohl ErrorMsg
  echon "Sorry, requires ruby support."
  finish
endif

" open command T with restriction
nnoremap ss :GS 

" erb partial
" open file under cursor
nnoremap gtf :ruby open_cursor_file<cr>
vnoremap <leader>np :ruby create_partial_by_selection<cr>
vnoremap <leader>nj :ruby create_js_partial_by_selection<cr>

" go to related files
nnoremap src :ruby open_related("css")<cr>
nnoremap srj :ruby open_related("js")<cr>
nnoremap srv :ruby open_related("views")<cr>
nnoremap sra :ruby open_related_controller<cr>

command! -nargs=1 FKC :ruby fk_css(<q-args>)

command! Stage :ruby detect_stage
command! DB :ruby enter_mysql_mode

command! -complete=custom,Stages -nargs=1 GS exec "ruby set_stage('<args>')"
function! Stages(A,L,P)
  return system("ls -1 app/controllers")
endfunction

" create new files
nnoremap <leader>nc :ruby create_new("css")<cr>
nnoremap <leader>nj :ruby create_new("js")<cr>
nnoremap <leader>nv :ruby create_new("views")<cr>
" new partial, trigger on erb
nnoremap <leader>np :ruby create_new("partial")<cr>

nnoremap <F11> :ruby terminator_exec("STAGE=#{get_stage} zsh")<cr>

let g:rubyapp_blame_url="http://dev.birdsage.com/projects/birdsage-media/repository/annotate/trunk"

"=========================================================================================
ruby << EOF
undef open_image_folder if respond_to? :open_image_folder
def open_image_folder
  cmd = "nautilus public/images/#{get_stage}"
  system cmd
end

undef fk_css if respond_to? :fk_css
def fk_css keyword
  fk keyword, "app/css/#{get_stage}"
end

undef opened? if respond_to? :opened?
def opened? title
 system("wmctrl -l | grep #{title} -q")
end

undef terminator_exec if respond_to? :terminator_exec
def terminator_exec command, title="loper command"
  vim_exec %Q{silent ! terminator -T '#{title}' -e '#{command}' &}
end

undef min_terminator_exec if respond_to? :min_terminator_exec
def min_terminator_exec command, title="loper command"
  terminator_exec "xdotool windowminimize `xdotool getactivewindow` ; #{command}", title
end

undef open_related if respond_to? :open_related
def open_related type
  t = { "css" => "scss", "views" => "erb" }
  ext      = t[type] || type
  path     = VIM::evaluate('expand("%")').split('/')
  project  = path[2].sub(/\..*/, '') rescue nil
  page     = path[3].sub(/\..*/, '') rescue nil
  subpage  = path[4].sub(/\..*/, '') rescue nil
  subpage  = path[5].sub(/\..*/, '') if subpage == "controllers" # js is source
  opath = case type
    when "controllers" then
      "app/#{type}/#{project}.rb"
    when "js" then
      "app/#{type}/#{project}/#{page}/controllers/#{subpage}.#{ext}"
    else
      "app/#{type}/#{project}/#{page}/#{subpage}.#{ext}"
  end
  VIM::command("tab drop #{opath}")
end

undef open_related_controller if respond_to? :open_related_controller
def open_related_controller
  VIM::command("tab drop app/controllers/#{get_stage}/#{get_stage}.rb")
end

undef get_stage if respond_to? :get_stage
def get_stage
  set_stage unless @stage
  @stage
end

undef set_stage if respond_to? :set_stage
def set_stage stage=nil
  path = "app/controllers/#{stage}"
  File.exist? path or stage = "shopadmin"
  @stage = stage || prompt("Switch To:")
  `echo #@stage > ~/.loper-stage`
  vim_exec "let g:loper_stage='#@stage'"
  vim_exec 'let g:rubyapp_prescript="STAGE=' + "'#@stage'" + '"'
  vim_exec 'let g:rubyapp_port="9999"'
  vim_exec 'let g:mysqlModeHost="127.0.0.1"'
  vim_exec 'let g:mysqlModePort="3306"'
  vim_exec 'let g:mysqlModeDBName="'+@stage+'"'
  vim_exec 'let g:mysqlModeUsername="root"'
  vim_exec 'let g:mysqlModePassword=""'
  puts "Stage: #@stage"
end

undef open_nerdtree if respond_to? :open_nerdtree
def open_nerdtree type=nil
  pwd = Dir.pwd
  if type
    p = pwd + "/app/#{type}/#{get_stage}"
  else
    p = pwd
  end
  VIM::command("NERDTree #{p}")
  VIM::command("cd #{pwd}")
end

undef open_file if respond_to? :open_file
def open_file type, global = false
  u = global ? type : "app/#{type}/#{get_stage}"
  #puts u
  VIM::command("CtrlP #{u}")
end

undef open_cursor_file if respond_to? :open_cursor_file
def open_cursor_file
  line = VIM::Buffer.current.line
  folder = "app/views" if line[/erb/]
  line = line.sub(/^[^"]*"/, '').sub(/".*$/, '')
  path = "#{folder}/#{line}.erb"
  if File.exists? path
    VIM::command "tab drop #{path}"
  else
    puts "#{path} not exist"
  end
end

undef create_partial_by_selection if respond_to? :create_partial_by_selection
def create_partial_by_selection
  path = VIM::evaluate('expand("%")').split('/')
  proj = path[2]
  layout = path[3].split('.')[0]
  base = "app/views/#{proj}/#{layout}/"
  name = prompt "Create partial in #{base}*.erb", "_header"
  filepath = base + name + ".erb"
  puts filepath

  call_line = %Q[<%= erb :"#{proj}/#{layout}/#{name}", locals: {} %>]
  vim_exec %Q[execute 'normal gvdO#{call_line}']
  vim_exec "tabe #{filepath}"
  vim_exec "normal p"
end

undef create_js_partial_by_selection if respond_to? :create_js_partial_by_selection
def create_js_partial_by_selection
  path = VIM::evaluate('expand("%")').split('/')
  proj = path[2]
  layout = path[3].split('.')[0]
  base = "app/js/#{proj}/#{layout}/"
  name = prompt "Create partial in #{base}*.js", "header"
  filepath = base + name + ".js"
  puts filepath

  vim_exec %Q[execute 'normal gvd']
  vim_exec "tabe #{filepath}"
  vim_exec "normal p"
end

undef detect_stage if respond_to? :detect_stage
def detect_stage
  VIM::command("echo 'Current stage is: #{get_stage}'")
end

undef create_new if respond_to? :create_new
def create_new type
  ext = case type
  when "css" then "scss"
  when "views" then "erb"
  when "partial" then "erb"
  else type
  end
  path   = VIM::evaluate('expand("%")').split('/')
  layout = path[3].sub(/\..*/, '') rescue ''
  name = path[4].sub(/\..*/, '') rescue ''
  # get layout
  if type == "partial"
    line = VIM::Buffer.current.line[/"(.*)"/, 1]
    _, layout, name = line.split('/')
  else
    layout = prompt "Layout:", layout
  end
  if layout && layout != ''
    name = prompt "Name([].#{ext}):", name
    if name && name != ''
      cat = type == "partial" ? "views" : type
      base_path = "app/#{cat}/#@stage/#{layout}"
      vim_exec "tab drop #{base_path}/#{name}.#{ext}"
      ext == "erb" or vim_exec "tab drop #{base_path}.#{ext}"
      if ext == "scss"
        vim_exec %Q[call append(line('.'), '@import "#{layout}/#{name}";')]
      end
    end
  end
  vim_exec "CtrlPClearAllCaches"
end

undef list_locale if respond_to? :list_locale
def list_locale
  cmd = %Q{find app/views/#{get_stage}/**/*.erb -exec egrep 't ".*"' {} \; | sed -e 's/.*t "//' -e 's/".*//'}
  `#{cmd}`.lines { |l|
    vim_exec "normal i #{l}"
  }
end
EOF
