source "https://rubygems.org"

gem 'rake'
gem 'log4r'                                              # event logging

# http user agent (browser) string parsing
# fetching browser type, version, platform, is mobile?
# can compare to supported browsers
gem 'useragent'

gem 'liquid'

gem 'sinatra'
gem 'sinatra_more'
gem 'sinatra-subdomain'
gem 'sinatra-contrib'
gem 'sinatra-support' # user agent helper, support browser.body_class & browser.iphone?
gem 'rack'
gem 'rack-contrib'

gem 'yajl-ruby', :require => 'yajl'                      # JSON parser/loader
#gem "json"

gem 'bindata'

#gem 'memcache-client'                                    # memcache client
gem 'pony'                                               # used for sending out email
gem 'excon'     # for frontend to talk to birdmin
# gem 'faraday'   # for frontend to talk to birdmin
gem 'savon'     # soap

gem 'image_sorcery'

gem 'i18n'
gem 'time-lord'

# MQ and caching
gem 'redis'

gem 'slim' # used in sidekiq console
gem 'sidekiq'
gem 'sidekiq-status'

gem 'faye', '0.8.6' # pubsub, websocket, used in the cube project, the game


gem 'spreadsheet'

gem 'omniauth'
gem 'omniauth-google-oauth2'
gem 'omniauth-facebook'
#gem 'google-api-client' # for translation

# payment gateway integration
# https://samurai.feefighters.com/developers/ruby/api-reference
#gem 'samurai'

# http://railscasts.com/episodes/288-billing-with-stripe
#gem 'stripe'

# http://www.codyfauser.com/2008/1/17/paypal-express-payments-with-activemerchant
gem 'activemerchant'
gem 'money'

# datamapper gems
gem 'activesupport'
gem 'dm-aggregates'
gem 'dm-constraints'
gem 'dm-core'
gem 'dm-migrations'
gem 'dm-serializer'
gem 'dm-timestamps'
gem 'dm-transactions'
gem 'dm-types', '1.2.0'
gem 'dm-validations'
gem 'dm-mysql-adapter'
gem 'dm-is-tree'                    # create tree structure in datamapper

#auto installed
#need libmysqlclient-dev
#gem 'mysql'

# previously, thin is installed via sys pack 'thin' on production
#gem 'thin', :git => 'git://github.com/macournoyer/thin.git'
gem 'thin'

gem 'eventmachine', :git => 'git://github.com/eventmachine/eventmachine.git'
gem 'nokogiri'

gem "prawn"
gem 'uglifier' # realtime uglify server generated javascript

group :production do
  gem "therubyracer"
end

# gem install ruby_core_source linecache19 ruby-debug-base19 ruby-debug19 ruby-debug-ide19 -- --with-ruby-include=$rvm_path/src/$(rvm tools strings)
group :development do
  # irb tools
  gem "fancy_irb"
  gem "awesome_print"
  gem "paint"
  gem "bond"
  gem "looksee"

  gem "hirb"
  gem "wirb"

  #gem "pdfkit"
  gem "sprockets"
  gem 'yui-compressor'
  # gem "compass"

  gem "sass"
  # gem "sass-css-importer"
  gem "sprockets-sass"

  #https://github.com/thoughtbot/neat
  #https://github.com/thoughtbot/bourbon

  # gem 'rmagick' # for rake resprite
  gem 'pry'
  gem 'pry-clipboard'
  gem 'pry-stack_explorer'
  gem 'pry-nav'
  gem 'capistrano'
  gem 'capistrano-ext'
  gem 'rvm-capistrano'
  gem 'sprite-factory'
  gem 'interactive_editor'
  gem "bourbon"
  gem "neat"
  # gem "puma"
  # gem "unicorn"

  # auto reload
  #gem 'guard-livereload'
  #gem 'rack-livereload'
end

group :test do
  gem 'libnotify'
  gem 'ffi'
  #gem 'livereload'
  gem 'rb-inotify'
  gem 'rspec'
end
