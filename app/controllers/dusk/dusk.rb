#encoding: utf-8
require 'sinatra/support'
#require_relative "member"
#require_relative "../../helpers/shopadmin"
require 'bindata'

# ssh-copy-id first
$proxy = "loki@192.168.40.25"
# $proxy = nil

$event = "/dev/input/event6"

class BoardResolver
  def initialize(board)
    @board = board
    @freezed = []
    5.times { |i|
      @freezed << [false, false, false, false, false, false]
    }
    @color = nil
    print_board
  end

  # 0, 1: bottom left 2
  # 2, 3: bottom right 2
  # 4, 5, 6, 7, 8: upper left 5
  # 9, 10, 11, 12, 13: upper right 5
  def gen_path
    combo_colors = []
    dependencies = [
      [], [0], [], [1, 2],
      [1], [4], [5], [3, 6], [7],
      [3], [9], [10], [0, 11], [13],
    ]
    starting = [
      [4, 0], [3, 0], [4, 5], [3, 5],
      [0, 0], [0, 1], [0, 2], [0, 3], [0, 4],
      [0, 5], [0, 4], [0, 3], [0, 2], [0, 1],
    ]
    done = ->(s) { freezed?(starting[s]) }
    while true
      oboard = backup_board
      # find the strategy with minimum cost
      strategy_cost = starting.length.times.map { |i|
        if done[i] || dependencies[i].any? { |k| !done[k] }
          999
        else
          pick_first_slot(starting[i]) # do nothing when it is not the first move
          r = resolve_combo(starting[i], i >= 9)
          o = r.nil? ? 999 : @steps.length
          restore_board(oboard)
          o
        end
      }
      break if strategy_cost.min >= 999 # all done
      strategy = strategy_cost.index(strategy_cost.min) # pick the strategy by cost
      pick_first_slot(starting[strategy]) # do nothing when it is not the first move
      r = resolve_combo(starting[strategy], strategy >= 9)
      combo_colors << @color if r
    end
    set_color(@current, @current_color)
    @current = [-1, -1]
    print_board
    ncombo, ncolor = count_combo
    [@steps, ncombo, ncolor]
  end

  private

  def count_combo
    combo = []
    6.times { |i| # upper
      a = 3.times.map { |k| get_color([k, i]) }
      combo << get_color([0, i]) if a.uniq.length == 1
    }
    [0, 3].each { |k| # lower
      [3, 4].each { |i|
        a = 3.times.map { |j| get_color([i, k+j]) }
        combo << get_color([i, k]) if a.uniq.length == 1
      }
    }
    [combo.length, combo.uniq.sort]
  end

  # rev: is the l shape block positioned in the opposite side
  def resolve_combo(first, rev=false)
    @color, _ = find_next_color(first)
    return nil if @color.nil?
    dir = 1
    if first[0] <= 2 # vertical, upper
      l, i = first
      s = rev ? i - 1 : i + 1
      rev = ->(a) { a }
    else
      i, l = first
      s = rev ? i + 1 : i - 1
      rev = ->(a) { a.reverse }
      dir = -1 if l > 2 # bottom, right
    end

    resolve_marks = ->(a, b, c, d, e, f, reverse_ab) {
      if reverse_ab
        resolve_target(b); resolve_target(a)
      else
        resolve_target(a); resolve_target(b)
      end
      if get_color(c) == @color # if the third slot is good already, just freeze the slot
        freeze_slot(c)
      elsif !d.nil? && !freezed?(d) # if not got blocked
        resolve_target(c)
      elsif @current == c && get_color(f) == @color # if blocked, but the current ball is inside
        resolve_target(c)
      else # do a L-shape first when cannot resolve
        resolve_target(e); move_ball_to(c)
        unfreeze_slot(b); resolve_target(c)
        unfreeze_slot(e); resolve_target(b)
      end
    }
    original_backup = backup_board
    ways = [
      # . e f . . .
      # a b c d . .
      [
        rev[[l, i]], rev[[l+dir, i]], rev[[l+2*dir, i]],
        rev[[l+3*dir, i]], rev[[l+dir, s]], rev[[l+2*dir, s]], false
      ],
      # f e . . . .
      # c b a . . .
      [
        rev[[l+2*dir, i]], rev[[l+dir, i]], rev[[l, i]],
        nil, rev[[l+dir, s]], rev[[l, s]], false
      ],
      # e f . . . .
      # b c a . . .
      [
        rev[[l+2*dir, i]], rev[[l, i]], rev[[l+dir, i]],
        nil, rev[[l, s]], rev[[l+dir, s]], true
      ],
      # . f e . . .
      # a c b . . .
      [
        rev[[l, i]], rev[[l+2*dir, i]], rev[[l+dir, i]],
        nil, rev[[l+2*dir, s]], rev[[l+dir, s]], true
      ],
    ]
    backups = []
    nsteps = []
    ways.each_with_index { |way, i|
      begin
        resolve_marks[*way]
        nsteps[i] = @steps.length
      rescue => ex
        nsteps[i] = 9999
      end
      backups[i] = backup_board
      restore_board(original_backup)
    }
    require 'pry'; binding.pry if nsteps.uniq.length == 1 && nsteps[0] == 9999
    i = nsteps.index(nsteps.min)
    restore_board(backups[i])
  end

  def backup_board
    Yajl::Encoder.encode(
      board: @board,
      freezed: @freezed,
      color: @color,
      current: @current,
      steps: @steps,
    )
  end

  def restore_board(meta)
    hash = Yajl.load(meta)
    @board = hash['board']
    @freezed = hash['freezed']
    @color = hash['color']
    @current = hash['current']
    @steps = hash['steps']
  end

  def get_color(slot)
    @board[slot[0]][slot[1]]
  end

  def set_color(slot, color)
    @board[slot[0]][slot[1]] = color
  end

  def freezed?(slot)
    @freezed[slot[0]][slot[1]]
  end

  def freeze_slot(slot)
    @freezed[slot[0]][slot[1]] = true
  end

  def unfreeze_slot(slot)
    @freezed[slot[0]][slot[1]] = false
  end

  def pick_first_slot(near)
    return if @current
    @color, _ = find_next_color(near)
    cc = [[], [], [], [], [], []]
    each_slot.each { |slot, color|
      next if color > 5
      cc[color] << slot
    }
    cc.delete_at(@color) # remove the color for the first combo
    cc_full = []
    cc.each { |c| cc_full.push(c) if c.length % 3 == 0 }
    cc.compact!
    cc.delete_if { |c| c.length % 3 == 0 }
    d = ->(a, b) { (a[0] - b[0]).abs + (a[1] - b[1]).abs }
    pool = cc.empty? ? cc_full.inject(:concat) : cc.inject(:concat)
    @current = pool.min_by { |slot| d[slot, near] }
    @steps = [@current.clone]
    @current_color = get_color(@current)
    set_color(@current, 9)
  end

  def resolve_target(target)
    slot = find_closest_color_ball(target, @color)
    if slot != target # not in correct place
      if target == @current &&
        ((target[0] - @current[0]).abs + (target[1] - @current[1]).abs) == 1
        path = [slot, target]
      else
        path = BFS.new.get_path(@board, @freezed, slot, target)
        raise "path not found" if path.nil?
      end
      resolve_path(path)
    end
    freeze_slot(target)
  end

  def resolve_path(path)
    path.each_with_index { |s, i|
      next if i == path.length - 1
      if @current != path[i+1]
        freeze_slot(s) # avoid moving the source
        current_path = BFS.new.get_path(@board, @freezed, @current, path[i+1])
        unfreeze_slot(s)
        if current_path.nil?
          raise "cannot resolve"
        end
        move_ball(current_path)
      end
      move_ball([@current, s])
    }
  end

  def move_ball_to(target)
    return if freezed?(target)
    return if @current == target
    path = BFS.new.get_path(@board, @freezed, @current, target)
    move_ball(path)
  end

  def move_ball(path)
    # Starting point is not the current location
    return nil if path.nil? || path[0] != @current
    path.each_with_index { |s, i|
      next if i == path.length - 1
      d = path[i + 1]
      @steps << d.clone
      t = get_color(s)
      set_color(s, get_color(d))
      set_color(d, t)
      @current = d
    }
  end

  def find_closest_color_ball(target, color)
    min_slot, min_slot_length = [], 999
    each_slot.each do |slot, c|
      next if freezed?(slot)
      next if color != c
      d = distance(slot, target)
      if d < min_slot_length
        min_slot_length, min_slot = d, slot
      end
    end
    min_slot
  end

  def distance(src, dest)
    return 0 if src.nil? || dest.nil?
    return 0 if src == dest
    path = BFS.new.get_path(@board, @freezed, src, dest)
    path.nil? ? 9999 : path.length
  end

  def find_next_color(target)
    cc = [[], [], [], [], [], []]
    each_slot.each do |slot, color|
      next if freezed?(slot)
      next if color > 5
      cc[color] << distance(slot, target)
    end
    cc = cc.map do |c|
      c.length >= 3 ? c.sort[0..2].inject(:+) : 999
    end
    return [nil, nil] if cc.min == 999
    # return color and the cost
    [cc.index(cc.min), cc.min]
  end

  # loop thru each slot and call block
  def each_slot
    Enumerator.new do |y|
      @board.each_with_index do |r, i|
        r.each_with_index do |c, k|
          y.yield([i, k], c)
        end
      end
    end
  end

  def print_board
    system('clear')
    colors = [91, 94, 92, 93, 90, 95]
    @board.each_with_index do |r, i|
      r.each_with_index do |c, k|
        color = colors[c]
        if @current == [i, k]
          print "●"
        else
          print "\033[#{color}m●\033[0m"
        end
        print ' '
      end
      puts ''
    end
  end
end

class BFS
  def get_path(board, freezed, slot, target)
    return nil if slot.empty?
    visited = Marshal.load(Marshal.dump(freezed))
    q, max_row, max_col = [slot], board.length, board[0].length
    visited[slot[0]][slot[1]] = true
    found = false

    directions = [ [-1, 0], [1, 0], [0, -1], [0, 1] ]
    while(!q.empty?)
      c = q.shift
      directions.each { |mr, mc|
        n = [c[0] + mr, c[1] + mc]
        next if n[0] < 0 || n[0] >= max_row
        next if n[1] < 0 || n[1] >= max_col
        next if visited[n[0]][n[1]] != false
        visited[n[0]][n[1]] = c
        if n == target
          found = true
          q.clear; break
        else
          q.push(n)
        end
      }
    end
    if found == false
      nil
    else
      output = [target.dup]
      while true
        o = output.last
        output << visited[o[0]][o[1]]
        break if output.last == slot
      end
      output.reverse
    end
  end
end

unless defined?(Event)
  class Event < BinData::Record
    endian :little
    # total: 8 + 2 + 2 + 4 = 16 bytes
    int64 :d
    uint16 :t
    uint16 :c
    int32 :v
  end
end

class Dusk < AppBase
  #helpers Sinatra::ShopadminHelper
  helpers Sinatra::UserAgentHelpers

  helpers do
    def get_room(roomid)
      key = "dusk-#{roomid}"
      json = Redis.new.get(key)
      return json if json.nil?
      Yajl.load(json)
    end

    def set_room(roomid, options)
      key = "dusk-#{roomid}"
      json = Yajl::Encoder.encode(options)
      Redis.new.set(key, json)
    end

    def rgb_to_hsl(r, g, b)
      r /= 255.0
      g /= 255.0
      b /= 255.0
      max = [r, g, b].max
      min = [r, g, b].min
      h = s = l = (max + min) / 2
      if max == min
        h = s = 0
      else
        d = max - min
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min)
        h = case max
          when r then (g - b) / d + (g < b ? 6 : 0)
          when g then (b - r) / d + 2
          when b then (r - g) / d + 4
        end
        h /= 6
      end
      [h, s, l].map { |i| (i * 255).round }
    end

    # recognition by hue
    # 0 - 5: fire, water, wood, light, dark, heart
    # 6 - 7: useless, poison
    def rgb_to_type7x6(r, g, b)
      hues = [12, 161, 141, 33, 200, 238, 170, 213]
      h, s, l = rgb_to_hsl(r, g, b)
      d = hues.min_by { |i| (i-h).abs }
      hues.index(d)
    end

    # recognition by hue
    def rgb_to_type6x5(r, g, b)
      fire, water, wood, light, dark, heart, useless, poison = [0, 1, 2, 3, 4, 5, 6, 7]
      h, s, l = rgb_to_hsl(r, g, b)
      # raise "Too small satuation, please retry" if s < 20
      if h < 8 && l > 150
        raise "Too small hue, please retry"
      end
      return useless if h < 9 && l < 62
      return poison if h > 200 && s > 200 && l < 20
      return useless if h > 230 && l < 50
      return useless if l < 30
      if h > 212 && h < 230 # either heart or dark
        return l < 230 ? dark : heart
      end
      hues = [
        23, # fire, 0
        163, # water, 1
        79, # wood, 2
        41, # light, 3
        216, # dark, 4
        235, # heart, 5
        247, # useless, 6
        200, # poison, 7
      ]
      d = hues.min_by { |i| (i-h).abs }
      t = hues.index(d)
      t
    end


    def adb(cmd)
      if $proxy
        ssh_cmd = "ssh #{$proxy}"
        @device ||= `#{ssh_cmd} /usr/local/bin/adb devices | grep -v 'emulator' | tail -n2 | head -n1 | awk '{print $1}'`.strip
        `#{ssh_cmd} /usr/local/bin/adb -s #{@device} "#{cmd}"`
      else
        @device ||= `adb devices | grep -v 'emulator' | tail -n2 | head -n1 | awk '{print $1}'`.strip
        `adb -s #{@device} #{cmd}`
      end
    end
  end

  before do
    @lang_code = request.host.split('.')[-3] # en.localhost.com
    @lang_code = "cht" unless %w{ cht chs en }.include? @lang_code
    lang_name = case @lang_code
    when "en" then "English"
    when "chs" then "Simplified Chinese"
    else "Traditional Chinese"
    end
    # @backend = BirdminMultiLang.new lang_name
  end

  not_found do
    #erb_out "main", "missing", "Missing | dusk"
  end

  get '/?' do
    erb_out "main", "login", "dusk"
  end

  def storage
    "/sdcard/"
  end

  def get_board7x6
    begin
      @board = adb("shell sh #{storage}dawn2")
      @board_rgb = @board.split("\n").map { |s| s.split(' ').map(&:to_i) }
      @board_rgb.each { |i| i.delete_at(-1) }
      @board_hsl = @board_rgb.map { |b| rgb_to_hsl(*b) }
      @board = @board_rgb.map { |b| rgb_to_type7x6(*b) }
    rescue => ex
      if ex.message == 'Too small satuation'
        sleep 0.1
        puts "Too small satuation, retry..."
        retry
      end
    end
  end

  def deploy_agents
    adb("push scripts/dawn2 #{storage}")
    adb("push scripts/dawn3 #{storage}")
    adb("shell sed -ie 's%-HOME-%#{storage}%' #{storage}dawn3")
    adb("push scripts/dusk-run-real.sh #{storage}")
  end

  def get_board6x5
    begin
      board = adb("shell sh #{storage}dawn3")
      @board_rgb = board.split("\n").map { |s| s.split(' ').map(&:to_i) }
      @board_rgb.each { |i|
        i.delete_at(-1)
      }
      @board_hsl = @board_rgb.map { |b| rgb_to_hsl(*b) }
      average_lightness = @board_hsl.map(&:last).inject(:+) / 30
      @board = @board_rgb.map { |b| rgb_to_type6x5(*b) }
      # fire 0, water 1, wood 2, light 3, dark 4, heal 5, useless 6, poison 7
      @board ||= %w[
        2 1 5 5 3 5
        5 1 2 5 3 3
        4 0 5 4 4 2
        2 1 2 3 4 1
        1 5 0 2 5 2
      ]
    rescue => ex
      if ['hue', 'satuation', 'Not ready yet'].any? { |m| ex.message.include?(m) }
        puts "#{ex.message}, 0.5s"
        sleep 0.5
        retry
      end
    end
  end

  get '/board7x6/?' do
    get_board7x6
    @type = '7x6'
    erb_out "main", "login", "dusk"
  end

  get '/board6x5/?' do
    get_board6x5
    @type = '6x5'
    erb_out "main", "login", "dusk"
  end

  get '/:roomid/?' do |roomid|
    @roomid = roomid
    @room = get_room(roomid)
    if roomid == 'random'
      @room = {
        'mode' => 'waiting',
        'board' => 5.times.map { |r| 6.times.map { |c| rand(0..5) } },
      }
    end
    redirect_to '/' if @room.nil?
    if @room['mode'] == 'applying'
      erb_out "main", "applying", "dusk"
    else
      erb_out "main", "index", "dusk"
    end
  end

  get '/api/:roomid/cmd/?' do |roomid|
    room = get_room(roomid)
    if room.nil?
      set_room(roomid, {
        mode: 'waiting',
        board: 5.times.map { |r| 6.times.map { |c| rand(0..5) } },
        path: [],
      })
      room = get_room(roomid)
    end
    if room['mode'] == 'waiting'
      'waiting'
    elsif room['mode'] == 'applying'
      set_room(roomid, room.merge('mode' => 'waiting'))
      steps = Yajl.load(room['path'])
      steps.unshift(steps.first.dup)
      steps = steps.map do |r, c|
        r.nil? || c.nil? ? nil : [8 + c * 177 + 88, 851 + r * 177 + 88]
      end.compact
      path = steps.flatten
      window = 50
      [window].concat(path).join(' ')
    end
  end

  get '/api/:roomid/submit-board?' do |roomid|
    room = get_room(roomid)
    if room
      colors = params['b'].split('-')
      board = 5.times.map { |i|
        6.times.map { |k| c = colors[i * 6 + k].to_i }
      }
      room['board'] = board
      set_room(roomid, room)
      redirect_to "/#{roomid}"
    end
  end

  post '/apply/?' do
    roomid = params['roomid']
    room = get_room(roomid)
    if room
      room['path'] = params['path']
      room['mode'] = 'applying'
      set_room(roomid, room)
    end
    redirect_back
  end

  post '/apply-7x6/?' do
    steps = Yajl.load(params['path'])
    steps = steps.map do |r, c|
      r.nil? || c.nil? ? nil : [8 + c * 147 + 73, 870 + r * 147 + 73]
    end.compact
    apply_path2(steps)
    redirect_to('/')
  end

  post '/apply-6x5/?' do
    if params['path'] == 'auto'
      get_board6x5
      resolver = BoardResolver.new(@board.each_slice(6).to_a)
      steps, ncombo, ncolor = resolver.gen_path
      redirect_path = "/?auto=1"
    else
      steps = Yajl.load(params['path'])
      redirect_path = "/"
    end
    steps = steps.map do |r, c|
      r.nil? || c.nil? ? nil : [8 + c * 177 + 88, 851 + r * 177 + 88]
    end.compact
    apply_path2(steps)
    redirect_to(redirect_path)
  end

  post '/deploy-agents/?' do
    deploy_agents
    redirect_back
  end

  def apply_path2(steps)
    interval = 8000
    split = 15
    ss = (177.0 / split).ceil

    time = 0
    x, y = steps[0]
    steps << steps.last
    steps << steps.last
    sl = steps.length
    data = []
    steps.each { |nx, ny|
      dx = nx > x ? ss : nx == x ? 0 : -ss
      dy = ny > y ? ss : ny == y ? 0 : -ss
      split.times { |i|
        time += interval
        time = interval if time > 99999
        if i == split - 1
          x = nx
          y = ny
        else
          x += dx
          y += dy
        end
        data << [time, 3, 55, 0]
        data << [time, 3, 54, y]
        data << [time, 0, 2, 0]
        time += interval
        data << [time, 3, 57, 0]
        data << [time, 3, 53, x]
        data << [time, 3, 58, 62]
        data << [time, 0, 0, 0]
      }
    }
    time += interval
    3.times { |i|
      data << [time, 0, 2, 0]
      data << [time, 0, 0, 0]
    }
    File.open("tmp_event", 'wb' ) do |output|
      output.write(data.flatten.pack("qSSl" * data.length))
    end
    if $proxy
      `scp ./tmp_event #{$proxy}:~/tmp_event`
    end
    adb("push ./tmp_event /sdcard/tmp_event")
    adb("shell 'sh /sdcard/dusk-run-real.sh > #{$event}'")
  end

  def apply_path(steps)
    steps.unshift(steps.first.dup)
    steps << steps[-1].dup
    obj = BinData::Array.new(:type => :event)
    output = []

    k = 6
    steps.each_with_index { |s, i|
      e = steps[i+1]
      next if e.nil?
      k.times { |i|
        x = s[0] + ((e[0] - s[0]).to_f / k * i).round
        y = s[1] + ((e[1] - s[1]).to_f / k * i).round
        output += [
          {:d=>0, :t=>3, :c=>57, :v=>0},
          {:d=>0, :t=>3, :c=>31, :v=>0},
          {:d=>0, :t=>3, :c=>29, :v=>x},
          {:d=>0, :t=>3, :c=>30, :v=>y},
          {:d=>0, :t=>3, :c=>58, :v=>50},
          {:d=>0, :t=>0, :c=>2, :v=>0},
          {:d=>0, :t=>0, :c=>0, :v=>0},
        ]
      }
    }
    output += [
      {:d=>0, :t=>0, :c=>2, :v=>0},
      {:d=>0, :t=>0, :c=>0, :v=>0},
    ]
    output.each { |h| obj << h }
    File.open("tmp_event", 'wb') { |io| obj.write(io) }
    adb("push tmp_event #{storage}")
    adb("push scripts/dusk-run-real.sh #{storage}")
    adb("push scripts/dusk-run.sh #{storage}")
    FileUtils.rm_rf('tmp_event')
    # adb("shell sh #{storage}dusk-run.sh")
  end

  post '/cancel/?' do
    roomid = params['roomid']
    room = get_room(roomid)
    if room
      room['mode'] = 'waiting'
      set_room(roomid, room)
    end
    redirect_back
  end

end
