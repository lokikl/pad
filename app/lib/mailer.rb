require 'pony'

# google mail is used
class MailSender

  # send mail via smtp (gmail)
  # expected:
  #   to, subject, body
  def self.send_mail params, smtp = nil
    smtp ||= CONFIG[:smtp]
    Log.inf "Sending mail, params => #{params}"
    Log.inf "Sending mail, smtp => #{smtp}"

    if params[:html_body] && params[:body].nil?
      # this line may fail if html_body contain chinese
      params[:body] = params[:html_body].gsub(/(<[^>]*>)|\t/, ' ')
    end

    Pony.mail(
      to:          params[:to],
      from:        params[:from] || smtp[:user_name],
      via:         :smtp,
      via_options: smtp,
      subject:     params[:subject],
      body:        params[:body],
      html_body:   params[:html_body],
      headers:     params[:headers],
      attachments: params[:attachments],
    )


  end

end






# deprecated




# deprecated since hard to avoid spamming
# localhost postfix can be used here, new approach
def mail opt
  Pony.mail(opt.merge(
    via:       :smtp,
    charset:   "utf-8",
    via_options: {
      address:              '127.0.0.1',
      port:                 '25',
      headers:              {
        'Content-Type' => 'text/html',
      },
    }
  ))
end

=begin
MailSender.send_mail(
  to: 'loki@lokinote.com',
  subject: 'hihi',
  body: 'body'
)

mail(
  to:         ["loki@birdsage.com"],
  from:       "Birdsage Media Limited <no-reply@theboss1.com>",
  subject:    "Test Sendmail",
  html_body:  "<h1>Big Title</h1><b>Test Sendmail Body",
)

=end
