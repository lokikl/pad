
require 'log4r'
require 'log4r/configurator'
require 'log4r/yamlconfigurator'
require 'log4r/outputter/datefileoutputter'

cfg = Log4r::YamlConfigurator
cfg['HOME'] = '.'
cfg.load_yaml_file('config/log4r.yml')
Log = Log4r::Logger['app'] 
