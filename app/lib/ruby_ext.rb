require 'cgi'

def print_backtraces
  puts caller.select { |d| d[/app/] }
end

class FalseClass
  def to_yn ; "No" ; end
end

class TrueClass
  def to_yn ; "Yes" ; end
end

class String
  def underscore
    self.to_s.gsub(/::/, '/').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
  end

  def escape
    gsub(/[^0-9a-zA-Z]+/, '-').downcase.chomp('-').sub(/^-*/, '')
  end

  def h
    CGI::escapeHTML self
  end

  def html_lines
    h.gsub /(?:\n\r?|\r\n?)/, '<br>'
  end

  def split_lines
    gsub(/\n/, '<br/>')
  end

  def ftime format
    DateTime.parse(self).strftime format
  end

  # May 25, 2014 at 2:43 pm
  def ftime_nice
    ftime "%B %-d, %Y at %l:%M %P"
  end

  # May 25, 2014
  def ftime_short
    ftime "%B %-d, %Y"
  end

  def truncate len=20
    length > len ? self[0..len] + "..." : self.dup
  end

  def truncate_word len=30
    words = split
    words.length > len ? words[0..len].join(' ') + "..." : self.dup
  end
end

class Hash
  def code
    self["name"].escape
  end

  # pass single or array of keys, which will be removed, returning the remaining hash
  def remove! *keys
    keys.each { |key| self.delete(key) }
    self
  end

  #non-destructive version
  def remove *keys
    self.dup.remove!(*keys)
  end

  def select_pairs keys
    reject { |k,v| !keys.include? k }
  end

  def reject_id
    reject { |k,_| k == :id }
  end

  def to_json
    Yajl::Encoder.encode self
  end

  def with_sym_keys
    self.inject({}) { |memo, (k,v)| memo[k.to_sym] = v; memo }
  end

end

class Array

  def find_name name
    find { |o| o["name"].escape == name }
  end

  def to_json
    Yajl::Encoder.encode self
  end
end

class Numeric

  def to_human_file_size
    units = %w{B KB MB GB TB}
    e = (Math.log(self)/Math.log(1024)).floor
    s = "%.3f" % (to_f / 1024**e)
    s.sub(/\.?0*$/, units[e])
  end

end

require 'time-lord'

class DateTime
  def timeago
    TimeLord::Period.new(self, Time.now).to_words
  end

  def dayago
    e = TimeLord::Period.new(self, Time.now)
    -e.difference / 1.day
  end

  # 3d ago / Today
  def dayago_str str="d ago"
    dayago == 0 ? "Today" : "#{dayago}#{str}"
  end

  def ymdhm d="."
    self.strftime "%Y#{d}%m#{d}%d %H:%M"
  end

  def ymd d="."
    self.strftime "%Y#{d}%m#{d}%d"
  end
end

module Color
  A_to_F = ["Aero", "Almond", "Amaranth", "Amazon", "Amber", "Amethyst", "Apricot", "Aqua", "Aquamarine", "Arsenic", "Artichoke", "Asparagus", "Auburn", "Aureolin", "AuroMetalSaurus", "Avocado", "Azure", "Bazaar", "Beaver", "Beige", "Bisque", "Bistre", "Bittersweet", "Black", "Blond", "Blue", "Blue-gray", "Blue-green", "Blueberry", "Bluebonnet", "Blush", "Bole", "Bone", "Boysenberry", "Brass", "Bronze", "Brown", "Brown-nose", "Bubbles", "Buff", "Burgundy", "Burlywood", "Byzantine", "Byzantium", "Cadet", "Camel", "Capri", "Cardinal", "Carmine", "Carnelian", "Catawba", "Ceil", "Celadon", "Celeste", "Cerise", "Cerulean", "Chamoisee", "Champagne", "Charcoal", "Chartreuse", "Cherry", "Chestnut", "Chocolate", "Cinereous", "Cinnabar", "Cinnamon", "Citrine", "Citron", "Claret", "Cobalt", "Coconut", "Coffee", "Copper", "Coquelicot", "Coral", "Cordovan", "Corn", "Cornsilk", "Cream", "Crimson", "Cyan", "Daffodil", "Dandelion", "Deer", "Denim", "Desert", "Desire", "Diamond", "Dirt", "Drab", "Ebony", "Ecru", "Eggplant", "Eggshell", "Emerald", "Eminence", "Eucalyptus", "Fallow", "Fandango", "Fawn", "Feldgrau", "Feldspar", "Flame", "Flattery", "Flavescent", "Flax", "Flirt", "Folly", "Fuchsia", "Fulvous"]
  G_to_M = ["Gainsboro", "Gamboge", "Ginger", "Glaucous", "Glitter", "Gold", "Goldenrod", "Grape", "Gray", "Gray-asparagus", "Gray-blue", "Green", "Green-yellow", "Grullo", "Harlequin", "Heliotrope", "Honeydew", "Iceberg", "Icterine", "Imperial", "Inchworm", "Independence", "Indigo", "Iris", "Irresistible", "Isabelline", "Ivory", "Jade", "Jasmine", "Jasper", "Jet", "Jonquil", "Keppel", "Khaki", "Kobe", "Kobi", "Lava", "Lavender", "Lemon", "Licorice", "Liberty", "Lilac", "Lime", "Limerick", "Linen", "Lion", "Liver", "Livid", "Lumber", "Lust", "Magenta", "Magnolia", "Mahogany", "Maize", "Malachite", "Manatee", "Mantis", "Maroon", "Mauve", "Mauvelous", "Melon", "Midori", "Mint", "Moccasin", "Mulberry", "Mustard"]
  N_to_Z = ["Navy", "Nyanza", "Ochre", "Olive", "Olivine", "Onyx", "Orange", "Orange-red", "Orchid", "Patriarch", "Peach", "Peach-orange", "Peach-yellow", "Pear", "Pearl", "Peridot", "Periwinkle", "Persimmon", "Peru", "Phlox", "Pink", "Pink-orange", "Pistachio", "Platinum", "Plum", "Popstar", "Prune", "Puce", "Pumpkin", "Purple", "Purpureus", "Quartz", "Rackley", "Rajah", "Raspberry", "Razzmatazz", "Red", "Red-brown", "Red-orange", "Red-violet", "Redwood", "Regalia", "Rhythm", "Rose", "Rosewood", "Ruber", "Ruby", "Ruddy", "Rufous", "Russet", "Rust", "Saffron", "Salmon", "Sand", "Sandstorm", "Sangria", "Sapphire", "Scarlet", "Seashell", "Sepia", "Shadow", "Shampoo", "Sienna", "Silver", "Sinopia", "Skobeloff", "Smitten", "Smoke", "Snow", "Soap", "Stizza", "Stormcloud", "Straw", "Strawberry", "Sunglow", "Sunray", "Sunset", "Tan", "Tangelo", "Tangerine", "Taupe", "Teal", "Telemagenta", "Tenn\303\251", "Thistle", "Timberwolf", "Tomato", "Toolbox", "Topaz", "Tulip", "Tumbleweed", "Turquoise", "Tuscan", "Tuscany", "Ube", "Ultramarine", "Umber", "Urobilin", "Vanilla", "Verdigris", "Vermilion", "Veronica", "Violet", "Violet-blue", "Violet-red", "Viridian", "Waterspout", "Wenge", "Wheat", "White", "Wine", "Wisteria", "Xanadu", "Yellow", "Yellow-green", "Zaffre", "Zomp"]
end

def country_names
  [ "Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua And Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh",
    "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada",
    "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark",
    "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia",
    "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary",
    "Iceland", "India", "Indonesia", "Iran, Islamic Republic of", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan",
    "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia, The Former Yugoslav Republic Of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania",
    "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua",
    "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar",
    "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia",
    "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo",
    "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Viet Nam", "Virgin Islands, British",
    "Virgin Islands, U.S.", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"
  ]
end

module RandomCode
  def self.code_dictionary
    @code_dictionary ||= [*'a'..'z'].concat([*'A'..'Z']).concat([*'0'..'9'])
  end

  def self.generate
    [*1..6].map { |i| code_dictionary.sample }.join
  end

  def self.password
    charset = %w{ 2 3 4 6 7 9 a c d e f g h j k l m n p q r t v w x y z A C D E F G H J K L M N P Q R T V W X Y Z}
    (0...8).map{ charset.to_a[rand(charset.size)] }.join
  end
end
