require 'redis'
require 'yajl'
require 'active_support'

module Cache

  def self.fetch(key, expire=nil, &block)
    content = cache.get key
    if content.nil?
      content = yield
      cache.set key, Yajl::Encoder.encode(content)
      expire.nil? or cache.expire key, expire
      content
    else
      content = Yajl.load content
    end
    content.is_a?(Hash) ?
      content.with_indifferent_access :
      content
  rescue => err
    Log.err "Redis fetch error: #{err.message}"
    yield
  end

  # Removes a cached item
  def self.delete key
    cache.del key
  end

  def self.read key
    content = cache.get key
    if content
      Yajl.load content
    else
      nil
    end
  end

  def self.write key, value
    cache.set key, Yajl::Encoder.encode(value)
  end

  # generate a cache key from a hash
  def self.gen_key prefix, opt
    key = Yajl::Encoder.encode opt
    "#{prefix}-#{key}"
  end

  # delete all keys
  def self.flush
    cache.flushdb
  end

  private

  def self.cache
    @@redis ||= Redis.new(:host => "127.0.0.1", :port => 6379)
  end

end
