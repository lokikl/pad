
require 'sinatra/base'
require 'sinatra/contrib'

Dir["app/lib/*.rb"].each { |f| require f }
require 'app/base'      # load the base app
require 'app/helpers'   # load global helpers

load_apps = [$stage.underscore]
load_apps << "loper" if $loper

load_apps.each { |app|
  require "app/controllers/#{app}/#{app}.rb"
}
