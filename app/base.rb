require 'sinatra_more/routing_plugin'
require 'sinatra_more/markup_plugin'
require 'sinatra/support'
require 'nokogiri' # for indexing


class AppBase < Sinatra::Base
  register SinatraMore::RoutingPlugin
  register SinatraMore::MarkupPlugin
  register Sinatra::Contrib
  helpers Sinatra::UserAgentHelpers

  # log route signature before executing the route block
  def route_eval
    Log.inf "    #{env['sinatra.route']}"
    throw :halt, yield
  end

  configure do
    set :root, Dir.pwd
    set :raise_errors, Proc.new { false }
    set :show_exceptions, false
    set :views, Proc.new { File.join(root, "app", "views") }
    set :partial_template_engine, :erb
    set :lock, true
    set :dump_errors, true
    set :logging, true
    #http://www.sinatrarb.com/configuration.html

    mime_type :json, 'application/json'
    mime_type :pdf, 'application/pdf'
    set :erb, :trim => '-'

    also_reload 'app/lib/*'
    also_reload 'app/models/*/*'
    also_reload 'db/schema.rb'
    also_reload 'config/*'

    unless $production
      require 'lib/autorefresh'
      use AutoRefresh

      Tilt::ERBTemplate.class_eval do
        def prepare
          @outvar = options[:outvar] || self.class.default_output_variable
          options[:trim] = '<>' if !(options[:trim] == false) && (options[:trim].nil? || options[:trim] == true)

          unless data.lines.first.start_with? "<!doctype"
            data and data.prepend <<-TAG
              <i class="erb-locator" style="display:none; visibility:hidden">#{file}</i>
            TAG
          end

          @engine = ::ERB.new(data, options[:safe], options[:trim], @outvar)
        end
      end
    end
  end

  before do
    @old_ie = browser.ie6? || browser.ie7? || browser.ie8?
    @new_ie = browser.ie? && !@old_ie
    @is_mobile = browser.iphone? || browser.rim? || browser.blackberry? ||
                 browser.android? || browser.nokia? || browser.ipad?
    @request_body = request.body.read
    @client_ip = request.env["HTTP_X_REAL_IP"] || request.env["REMOTE_ADDR"]
    @request_at = Time.now
    Log.inf " => #{env["REQUEST_METHOD"]} #{env["REQUEST_PATH"]}"

    $dev = session[:user_dev] == true
  end

  after do
    process_time = (Time.now - @request_at).round(4)
    Log.inf "    #{response.status} - #{process_time}s"

    if @index_as
      doc = Nokogiri.HTML(response.body.join)
      lines = doc.at('#page').inner_text.lines.to_a.map(&:strip)
      lines.delete ""
      lines.uniq!
      lang_code = @index_lang_code || @lang_code # leave room for hacking, like multi tenant
      r = Birdmin.call_api("search/index", {
        lang_code: lang_code,
        page_name: @index_as,
        path:      request.path,
        content:   lines.join("\n"),
      }) rescue nil
    end
  end

  error do
    error_page = nil
    error_page = on_error if respond_to? :on_error
    err = env['sinatra.error']
    full = err.message
    err.backtrace.each { |e|
      full << "\n                         " << e.sub(/^.*trunk\//, '') if e[/trunk/] || e[/current/]
    }
    Log.err " !! #{full}"

    if CONFIG[:birdmin_url]
      Birdmin.log_error request, err, {
        request_body: @request_body,
        client_ip: @client_ip,
        request_at: @request_at,
      }
    end

    status 500
    if error_page
      error_page
    else
      err.message
    end
  end

  not_found do
    "Loper: Resource not found"
  end

  get '/api/start-inspect/?' do
    session[:user_dev] = true
    redirect '/'
  end

  get '/api/stop-inspect/?' do
    session[:user_dev] = false
    redirect '/'
  end

  # get '/api/cache/clear/?' do
  #   Birdmin.clear_cache
  #   redirect "/"
  # end

  # get '/api/cache/invalidate/:cat_code/?' do |cat_code|
  #   Birdmin.invalidate_category cat_code
  #   redirect "/"
  # end

  # curl -XPOST --form "json=@giormani.json" http://localhost.com:9999/api/deploy
  post '/api/deploy/?' do
    if params["json"]
      # from pull-xxx shortcut
      Cache.delete "birdmin-packed-hash"
      json = Yajl.load params["json"][:tempfile].read
    else
      # from cms push changes api
      json = request_json
    end
    Log.inf " /api/deploy json include languages: #{json.keys.join(',')}"
    Cache.delete("birdmin-store-settings")
    Birdmin.runtime_cache.clear
    Birdmin.invalidate_by_hash json
    true
  end

  # curl -u'1234:1234' -XPOST http://bdemo.info/api/retrieve_cache > giormani.json
  post '/api/retrieve_cache/?' do
    halt_json Birdmin.packed_hash
  end

  if !$production || CONFIG[:cms_image]
    get '/system/product/*' do |image_name|
      redirect "#{CONFIG[:birdmin_url]}/system/product/#{image_name}"
    end
  end
end
