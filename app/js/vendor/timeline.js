// imagesuploaded.js is required
$(function() {

  //timeline
    //left, right
      //node
  var $timeline, $left, $right, nodes = [];

  function fetchObjects() {
    $timeline = $('div.timeline');
    if (!!$timeline.length) {
      $left = $('div.left', $timeline);
      $right = $('div.right', $timeline);
      var lnodes = $('div.node', $left);
      var rnodes = $('div.node', $right);
      for (var i = 0; i < lnodes.length; i++) {
        nodes.push(lnodes[i]);
        if (rnodes[i]) nodes.push(rnodes[i]);
      }
    } else {
      $timeline = null;
    }
  }

  function repositionTimelineNodes() {
    if (!$timeline) return;
    console.log('repositioning');
    $timeline.imagesLoaded().always(function() {
      for (var i = 1; i < nodes.length; i++){
        var $this = $(nodes[i]);
        var pnodeTop = $(nodes[i-1]).offset().top;
        var thisTop = $this.offset().top;
        var diff = thisTop - pnodeTop - 40;
        if (diff < 0) {
          $this.css('padding-top', -diff);
        } else {
          $this.css('padding-top', 0);
        }
      }
    });
  }

  $(window).on('statechangecomplete', function() {
    if (!$('#ie6, #ie7, #ie8').length) {
      fetchObjects();
      repositionTimelineNodes();
    }
  });

  $(window).off('resize.timeline').on('resize.timeline', function() {
    if (!$('#ie6, #ie7, #ie8').length) {
      doFrame(repositionTimelineNodes);
    }
  });

});
