$(function() {

  var $window = $(window);

  function scrolled() {
    $('[data-affix]').each(function() {
      var $this = $(this);
      var offset = $this.data('affix'); // || $this.offset().top;
      var postfix = $this.data('postfix'); // || $this.offset().top;
      var hasClass = $this.hasClass('detached');
      if (postfix) {
        var distanceToBottom = $('body').height() - $(window).scrollTop();
        if (postfix > distanceToBottom) {
          // TODO: attach using fixed position + bottom instead of top here
        } else {

        }
      }
      var forceDetached = $('body').hasClass('no-affix');
      if (forceDetached || !hasClass && $window.scrollTop() > offset) {
        $this.addClass('detached');
        //$this.data('offset', offset);
      } else if (hasClass && $window.scrollTop() <= offset) {
        $this.removeClass('detached');
        //$this.data('offset', null);
      }
    });
  } // end of resized

  $window
    .off('scroll.affix')
    .on('scroll.affix', function() {
      doFrame(scrolled);
    })
    .trigger('scroll.affix');

  $window.on('statechangecomplete', function() {
    $window.trigger('scroll.affix');
  });

});
