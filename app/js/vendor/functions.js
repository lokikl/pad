
// functions
var propVal = function(p) {
  return function(object) {
    return object[p];
  };
}
var isEqualToValueOf = function(key, value) {
  return function(object) {
    return object[key] === value;
  };
}
var isNotEqualToValueOf = function(key, value) {
  return function(object) {
    return object[key] != value;
  };
}
var sumProp = function(prop, prop2) {
  return function(sum, e) {
    var v = e;
    if (prop) v = v[prop];
    if (prop2) v = v[prop2];
    return sum + v;
  }
}
var set = function(prop, value) {
  return function(e) {
    e[prop] = value;
  }
}
var isUndefined = function(prop) {
  return function(object) {
    return typeof(object[prop]) === "undefined";
  }
}

function scrollToTop() {
  $("html, body").animate({ scrollTop: 0 }, 0);
}

var doFrame = (
  window.requestAnimationFrame       ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame    ||
  window.oRequestAnimationFrame      ||
  window.msRequestAnimationFrame     ||
  function(callback) {
    setTimeout(callback, 1000 / 60);
  }
);

function getParam(name) {
  var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function rand(start, end) { // 3, 6
  var range = end + 1 - start;
  return Math.floor(Math.random() * range) + start;
}

function fetchNaturalSize(images, callback) {
  var remain = images.length;
  var finished = function() {
    remain -= 1;
    if (remain === 0 && callback) callback();
  }
  images.each(function() {
    var img = $(this);
    if (img.data('width')) return finished();
    var image = new Image();
    $(image).load(function() {
      img.data({
        width: this.width, height: this.height,
        ratio: this.width / this.height,
      });
      finished();
    });
    image.src = img.prop('src') || img.css('backgroundImage').match(/url\((.*)\)/)[1];
  });
}

function addMonth(date, n) {
  if (!n) n = 1;
  date.setMonth(date.getMonth() + n);
}

// Array.prototype.insert = function (index, item) {
//   this.splice(index, 0, item);
// };

// turn image width and height to css style for jquery to call
// auto reduce size when image is larger then the screen
function correctDimension(imgWidth, imgHeight, pad) {
  pad = pad || 0.9;
  var maxWidth = $(document).innerWidth() * pad;
  var maxHeight = $(document).innerHeight() * pad;
  var ratio = imgWidth / imgHeight;
  if (imgHeight > maxHeight) {
    imgHeight = maxHeight;
    imgWidth = imgHeight * ratio;
  }
  if (imgWidth > maxWidth) {
    imgWidth = maxWidth;
    imgHeight = imgWidth / ratio;
  }
  return {
    width: imgWidth,
    height: imgHeight,
    marginTop: -imgHeight / 2,
    top: "50%",
  };
}

function askConfirm(q, callback) {
  apprise(q, {confirm:true}, function(r) {
    if (r) callback(r);
  });
}


// base class extension
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
// string.trim();
if (typeof String.prototype.trim != 'function') { // detect native implementation
  String.prototype.trim = function () {
    return this.replace(/^\s+/, '').replace(/\s+$/, '');
  };
}

var newPage = function(name) {
  return {
    template: Templates[name],
    controller: window[name.capitalize() + 'Ctrl']
  };
}

var url = function(link) {
  if (!link) link = "";
  var sitePath = document.body.attributes.getNamedItem('site-path').value;
  return sitePath + link;
}

function preloadImages(images, callback) {
  imageCache.pushArray(images, null, callback);
}

// helpers to do nicer http handling in angular
var showSavingMessage = function(load) {
  var message = "Saving, please wait...";
  if (load) message = "Loading, please wait...";
  $.blockUI({
    message: message,
    css: {
      border: 'none',
      padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    }
  });
};

var hideSavingMessage = function() {
  $.unblockUI();
}

var getContent = function(http, url, callback, noblockui) {
  httpContent(http, url, null, callback, 'get', noblockui);
}

var saveContent = function(http, url, payload, callback) {
  httpContent(http, url, payload, callback, 'post');
}

var httpContent = function(http, url, payload, callback, verb, noblockui) {
  if (!noblockui) showSavingMessage();
  http[verb](url, payload)
    .success(function(data, status) {
      $.unblockUI();
      if (callback) callback(data);
    })
    .error(function(data, status) {
      if (data.search('Permission Denied') === 0) {
        window.location.reload();
      } else {
        alert('failed to save because ' + data);
      }
      if (!noblockui) $.unblockUI();
    });
}

function handleNotifications() {
  var notification = $('div.notification');
  if (notification.length > 0) {
    apprise(notification.text());
    notification.remove();
  }
}

function elementTop(dom) {
  var obj = $(dom);
  return obj.offsetParent().scrollTop() + obj.position().top;
}

// my scroll spy implementation
function scrollSpy(pane, item) {
  var detectScroll = function() {
    var center = $(pane).scrollTop() + 350;
    var items = $(item);
    var tid = items.length - 1;
    items.each(function(i) {
      var targetTop = elementTop(this.hash);
      if (center < targetTop) {
        tid = i - 1;
        return false;
      }
    });
    var target = $(items[tid]);
    if (!target.hasClass('highlight')) {
      items.removeClass('highlight');
      target.addClass('highlight');
    }
  }
  $(pane).on('scroll.scrollspy', function() {
    $.doTimeout('scroll', 200, detectScroll);
  });
  $(item).first().addClass('highlight');
}


// angular directive
var AddModule = {
  extlink: function(app) {
    app.directive('extlink', function() {
      return {
        link: function(scope, element, attrs, controller) {
          element.on('click', function() {
            window.location.assign(element.prop('href'));
          });
        }
      };
    });
  },
  uploadImage: function(app) {
    app.directive('uploadImage', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
          if (controller != null) {
            element.change(function() {
              var f = this.files[0];
              try {
                var reader = new FileReader();
                reader.onloadend = function(e) {
                  var img = e.target.result;
                  controller.$setViewValue({f:f,img:img});
                  scope.$digest();
                }
                reader.readAsDataURL(f);
              } catch (err) {
                console.log(err);
                return false;
              }
            });
          }
        }
      };
    });
  },
  tooltip: function(app) {
    app.directive('tooltip', function() {
      return {
        link: function(scope, element, attrs, controller) {
          var msg = attrs.tooltip;
          element.tooltip({title: msg});
        }
      }
    });
  },
  datepicker: function(app) {
    app.directive('datepicker', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
          if (controller != null) {
            var originalRender = controller.$render;
            controller.$render = function() {
              originalRender();
              var date = controller.$viewValue;
              if (!element.data('datepicker')) {
                element.datepicker()
                  .on('changeDate', function(ev) {
                    var mydate = ev.date.toString();
                    controller.$setViewValue(mydate);
                    if (!scope.$$phase) scope.$apply();
                  });
              }
              element.datepicker('setValue', new Date(date));
            };
          }
        }
      };
    });
  },
  datetime: function(app) {
    app.directive('datetime', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
          if (controller != null) {
            var originalRender = controller.$render;
            controller.$render = function() {
              originalRender();
              var date = controller.$viewValue;
              if (date) {
                if (typeof date === "string") date = new Date(date);
                if (!element.data('datetimeEntry')) {
                  element.datetimeEntry({
                    defaultDatetime: date,
                    datetimeFormat: 'D n Y h:Ma',
                    spinnerImage: '',
                  });
                  element.change(function(e) {
                    if (!controller.$viewValue) return false;
                    var date = element.datetimeEntry("getDatetime");
                    if (date) {
                      controller.$setViewValue(date);
                      if (!scope.$$phase) scope.$apply();
                    }
                  });
                }
                element.datetimeEntry("setDatetime", date);
              }
            };
          } else {
            element.change(function(e) {
              var d = new Date($(this).val());
              if (d != "Invalid Date") {
                element.datetimeEntry("setDatetime", d);
              }
            });
            element.datetimeEntry({
              datetimeFormat: 'D n Y h:Ma',
              spinnerImage: '',
            }).change();
          }
        }
      };
    });
  },
  scrollpane: function(app) {
    app.directive('scrollpane', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
          if (controller != null) {
            var originalRender = controller.$render;
            controller.$render = function() {
              originalRender();
              var album = controller.$viewValue;
              if (album) {
                var api = element.data('jsp');
                var html = album.photos.map(function(p) {
                  return '<a class="photo" href="#" style="background-image:url(' + p.th_url + ')"></a>';
                }).join();
                api.getContentPane().html(html);
                api.reinitialise();
              }
            };
          }
          element.jScrollPane({
            verticalDragMinHeight: 20,
            verticalDragMaxHeight: 20,
            horizontalDragMinWidth: 20,
            horizontalDragMaxWidth: 20
          });
        }
      };
    });
  },
  jcrop: function(app) {
    app.directive('jcrop', function() {
      return {
        require: '?ngModel',
        link: function(scope, element, attrs, controller) {
          if (controller != null) {
            var api;
            var originalRender = controller.$render;
            controller.$render = function() {
              originalRender();
              var cropRegion = controller.$viewValue;
              if (cropRegion) {
                setTimeout(function() {
                  element.Jcrop({
                    aspectRatio: $eventImgRatio,
                    onChange: function(c) {
                      cropRegion[0] = c.x;
                      cropRegion[1] = c.y;
                      cropRegion[2] = c.w;
                      cropRegion[3] = c.h;
                    },
                  }, function() {
                    api = this;
                  });
                  var x = cropRegion[0];
                  var y = cropRegion[1];
                  api.setSelect([x, y, x+cropRegion[2], y+cropRegion[3]]);
                }, 100);
              }
            };
          }
        }
      }
    });
  },
  newlines: function(app) {
    app.filter('newlines', function() {
      return function(input) {
        return input ? input.split("\n") : [];
      }
    });
  },
  unless: function(app) {
    app.filter('unless', function() {
      return function(input, r) {
        return input ? input : r;
      }
    });
  },
  cssbg: function(app) {
    app.filter('cssbg', function() {
      return function(url) {
        return url ? 'background-image: url('+url+')' : '';
      }
    });
  },
  gravatar: function(app) {
    app.filter('gravatar', function() {
      return function(input) {
        var url = "http://www.gravatar.com/avatar/";
        return input ? url+input+"?d=mm" : url+"dummmy?d=mm";
      }
    });
  },
};

// workaround for browser without .forEach
if (!Array.prototype.forEach) {
  Array.prototype.forEach = function (fn, scope) {
    'use strict';
    var i, len;
    for (i = 0, len = this.length; i < len; ++i) {
      if (i in this) {
        fn.call(scope, this[i], i, this);
      }
    }
  };
}
// workaround for browser without .indexOf
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(obj, start) {
    for (var i = (start || 0), j = this.length; i < j; i++) {
      if (this[i] === obj) { return i; }
    }
    return -1;
  }
}

// gracefully handle console.log when there isn't (IE8)
if (!window.console) {
  var noOp = function(){}; // no-op function
  console = {
    log: noOp,
    warn: noOp,
    error: noOp
  }
}

