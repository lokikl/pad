$(document).ready(function () {
  var $body = $('body');

  function openSharingDialog(url, h, w) {
    var opt = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height='
      + h + ',width=' + w;
    window.open(url, '', opt);
  }

  function getURL() {
    var url = window.History.getPageUrl();
    return encodeURIComponent(url);
  }

  $body.on('click', 'a.share-facebook', function() {
    var url = "http://www.facebook.com/sharer.php?u=" + getURL();
    url += "&t=" + encodeURIComponent(document.title);
    openSharingDialog(url, 300, 600);
    return false;
  });

  $body.on('click', 'a.share-twitter', function() {
    var url = "https://twitter.com/share?url=" + getURL();
    url += "&text=" + encodeURIComponent(document.title);
    openSharingDialog(url, 300, 600);
    return false;
  });

  $body.on('click', 'a.share-googleplus', function() {
    var url = "https://plus.google.com/share?url=" + getURL();
    openSharingDialog(url, 350, 480);
    return false;
  });

});
