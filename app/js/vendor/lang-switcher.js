/*
<span class="lang-switch">
  <a href="javascript:void(0)" data-lang-switch="cht">中文</a>
  <span> | </span>
  <a href="javascript:void(0)" data-lang-switch="en">ENG</a>
</span>
*/

// jquery is required
$('body').on('click', 'a[data-lang-switch]', function() {
  var $this = $(this);
  var lang = $this.data('lang-switch');
  var host = window.location.host;
  if (["cht", "chs", "en", "www"].indexOf(host.split('.')[0]) > -1) {
    host = host.replace(/[^.]+\./, lang + '.');
  } else {
    host = lang + "." + host;
  }
  var url = window.location.protocol + "//" + host + window.location.pathname;
  window.location.assign(url);
});


$(window).on('statechangecomplete', function() {
  $('.locale-tip').tooltip({
    html: true,
    placement: 'top',
    container: 'body',
    // selector: 'body',
    delay: { show: 0, hide: 2000 }
  });
});

$('body').on('mouseenter', '.locale-tip', function() {
  $(this).css('opacity', 0.5);
});

$('body').on('mouseleave', '.locale-tip', function() {
  $(this).css('opacity', 1);
});
