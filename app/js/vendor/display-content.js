
$(function() {
  $('body').on('click', 'a[data-display-content]', function() {
    var $this = $(this);
    var sel = $this.data('display-content');
    var $target = $(sel);
    if ($this.data('close')) {
      $this.remove(0);
    }
    $target.slideDown(800);
    if (!$this.data('no-slide')) {
      $('body,html')
        .stop(true, true)
        .animate({scrollTop:$target.offset().top}, 1000);
    }
  });

  function hideContent() {
    $('a[data-display-content]').each(function() {
      var $this = $(this);
      var sel = $this.data('display-content');
      var $target = $(sel);
      $target.hide(0);
    });
  }

  $(window).on('statechangecomplete', function() {
    hideContent();
  });

});
