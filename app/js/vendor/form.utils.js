
$(function() {

  $('body').on('submit', 'form', function() {
    $('input[type=password][data-encrypt]', $(this)).each(function() {
      var $password = $(this);
      var password = hex_sha512($password.val());
      var id = $password.data('encrypt');
      $('#' + id).val(password);
    });
  });

  $('body').on('click', 'form .btn.submit, form .mybtn.submit', function() {
    var text = $(this).data('confirm');
    var pass = true;
    if (text) {
      pass = confirm(text);
    }
    if (pass) $(this).parents('form').submit();
  });

  $('body').on('click', 'a[data-method]', function() {
    var $this = $(this);
    var method = $this.data('method');
    var text = $(this).data('confirm');
    if (!text || confirm(text)) {
      var url = $this.data('href') || $this.attr('href');
      var form = $('<form action="' + url + '" method="' + method + '"></form>');
      $('body').append(form);
      $(form).submit();
    }
    return false;
  });

});
