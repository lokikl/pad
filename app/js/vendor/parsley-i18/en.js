// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

// Define then the messages
window.ParsleyConfig.i18n.en = $.extend(window.ParsleyConfig.i18n.en || {}, {
  defaultMessage: "This information seems to be invalid.",
  type: {
    email:        "This information should be a valid email.",
    url:          "This information should be a valid url.",
    number:       "This information should be a valid number.",
    integer:      "This information should be a valid integer.",
    digits:       "This information should be digits.",
    alphanum:     "This information should be alphanumeric."
  },
  notblank:       "This information should not be blank.",
  required:       "This information is required.",
  pattern:        "This information seems to be invalid.",
  min:            "This information should be greater than or equal to %s.",
  max:            "This information should be lower than or equal to %s.",
  range:          "This information should be between %s and %s.",
  minlength:      "This information is too short. It should have %s characters or more.",
  maxlength:      "This information is too long. It should have %s characters or less.",
  length:         "Length of this information should be within %s and %s characters.",
  mincheck:       "You must select at least %s choices.",
  maxcheck:       "You must select %s choices or less.",
  check:          "You must select between %s and %s choices.",
  equalto:        "This information should be the same."
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator)
  window.ParsleyValidator.addCatalog('en', window.ParsleyConfig.i18n.en, true);
