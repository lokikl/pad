//
// navy
//   - no nothing when requesting url is identical to the current one
//   - default is replacing state
//   - navy-skip: traditional navigation
//   - navy-base: default target
//   - navy-land: add new state
//   - data-navy-aimed: jq query, multiple allows, match occurrences one by one, default to body
//   - whenever error occurred, alert as this is a design issue
//
(function(window,undefined){
  $(function(){
    var $body = $('body');

    function changeDocumentTitle(title) {
      if (title === '') { return; }
      window.document.title = title;
      try {
        var titleHTML = window.document.title
          .replace('<','&lt;')
          .replace('>','&gt;')
          .replace(' & ',' &amp; ');
        window.document.getElementsByTagName('title')[0].innerHTML = titleHTML;
      }
      catch ( Exception ) { }
    }

    // Internal Helper
    // $('a:internal') to select all internal links
    $.expr[':'].internal = function(obj, index, meta, stack){
      var url = $(obj)[0].href || '';
      var rootUrl = window.History.getRootUrl();
      return url.substring(0,rootUrl.length) === rootUrl;
    };

    function render(html, title, url, aimed) {
      $(aimed).html('<p>Rendering...</p>');
      // console.log("RENDERING");
      // console.log("title: " + title);
      // console.log("url: " + url);
      // console.log("aimed: " + aimed);
      var $page = $(html);
      changeDocumentTitle(title);

      var $newbody = $('<div></div>');
      $newbody.html($page.filter('#site-wrap'));
      var $originals = $(aimed);
      var allFound = true;
      $(aimed, $newbody).each(function(i) {
        var $node = $(this);
        var $target = $($originals[i]);
        if ($target.length > 0) {
          $target.replaceWith($node);
        } else {
          allFound = false;
          return false;
        }
      });
      if (!allFound) {
        console.log('not all target found, so render to navy base');
        render(html, title, url, '.navy-base');
      }
    }

    var target = 'a:internal:not(.navy-skip):not([target=_blank])';
    $body.on('click', target, function(event) {
      // Continue as normal for cmd clicks etc, open new tabs...
      if ( event.which == 2 || event.metaKey ) { return true; }
      var $node = $(this);
      var action = $node.hasClass('navy-land') ? 'pushState' : 'replaceState';
      var url = $node.attr('href');
      // var currentUrl = location.pathname + location.search;
      // if (url === currentUrl) return false;
      if (url[0] != '#') {
        event.preventDefault();
        // original href will be treated as a backup plan (nojs set)
        var aimed = $node.data('navy-aimed') || '.navy-base';
        $(aimed).addClass('navy-aimed');
        $.ajax({
          url: url,
          success: function(html, textStatus, jqXHR){
            var $page = $(html);
            var title = $page.filter('title').text();
            window.History[action]({
              html: html, title: title, url: url,
              aimed: aimed,
              ts: (new Date()).getTime(), // to ensure statechange will be triggered
            }, title, url);
          },
          error: function(jqXHR, textStatus, errorThrown){
            var errMsg = jqXHR.responseText;
            alert(url + '\n\n' + errMsg);
          }
        });
        return false;
      }
    }); // end of the click listener

    $body.on('submit', 'form:not(.navy-skip)', function(event) {
      var $form = $(this);
      var url = $form.prop('action');
      var aimed = $form.data('navy-aimed') || '.navy-base';
      $(aimed).addClass('navy-aimed');
      $form.ajaxSubmit({
        url: url,
        success: function(html) {
          var $page = $(html);
          var title = $page.filter('title').text();
          window.History.replaceState({
            html: html, title: title, aimed: aimed,
            ts: (new Date()).getTime(), // to ensure statechange will be triggered
          }, title, null);
        },
        error: function(jqXHR) {
          var errMsg = jqXHR.responseText;
          alert(url + '\n\n' + errMsg);
        }
      });
      return false;
    }); // end of the form submit listener

    $(window).bind('statechange',function(e) {
      var s = History.getState();
      if (s) {
        var d = s.data;
        if (d && d.title) {
          render(d.html, d.title, d.url, d.aimed);
          $(window).trigger('statechangecomplete');
        } else {
          window.location.reload();
        }
      }
    });

    $body.on('click', '[data-confirm]', function() {
      var $this = $(this);
      var text = $(this).data('confirm');
      if (!confirm(text)) return false;
    });

  }); // end onDomLoad
})(window);

