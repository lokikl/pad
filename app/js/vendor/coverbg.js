$(function() {

  var $window = $(window);

  function resized() {
    var winHeight = $window.innerHeight();
    $('[data-fullscreen]').each(function() {
      var $this = $(this);
      if ($this.data('fullscreen')) {
        $this.css('min-height', winHeight);
        $this.trigger('resize.backstretch');
      }
      var $content = $('> .vertical-center', $this);
      if (!!$content) {
        var contentHeight = $content.height();
        var contentTop = (winHeight - contentHeight) / 2;
        $content.css('top', contentTop);
      }
    });
  } // end of resized

  window.initCoverbg = function() {
    $('[data-coverbg]').each(function() {
      var $this = $(this);
      var bgUrl = $this.data('coverbg');
      var h = { fade: 750 }
      var cY = $this.data('centery');
      if (typeof(cY) !== 'undefined') {
        h.centeredY = cY;
      }
      var cX = $this.data('centerx');
      if (typeof(cX) !== 'undefined') {
        h.centeredX = cX;
      }
      if (bgUrl) {
        $this.backstretch(bgUrl, h);
      }
    });

    $window
      .off('resize.fullscreen')
      .on('resize.fullscreen', function() { doFrame(resized); })
      .trigger('resize.fullscreen');
  }

  $window.on('statechangecomplete', window.initCoverbg);

});
