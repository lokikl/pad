//= require vendor/functions
//= require vendor/sha
//= require vendor/jquery-1.8.2
//= require vendor/bootstrap.3.0.0
//= require vendor/underscore-min
//= require vendor/fastclick
//= require vendor/form.utils
//= require vendor/parsley-2.0.0
//= require vendor/jquery.backstretch.min
//= require vendor/coverbg
//= require vendor/toggle
//= require vendor/tabtoggle
//= require vendor/imagesloaded
//= require vendor/jquery.apprise.1.5.min
//= require vendor/jquery.nivo.slider
//= require vendor/coverbg
//= require vendor/hideshow

//= require vendor/history
//= require vendor/history.html4
//= require vendor/history.adapter.jquery
//= require vendor/json2
//= require vendor/lframe

//= require_self

'use strict';

$(function() {
  var $body = $('body');
  var $board = $('div.board');
  var $steps = $('#f_steps');
  var $pointer = $('#pointer');
  var $applyBtn = $('#apply-btn');
  var is7x6 = $board.data('type') === '7x6';
  var nCol = is7x6 ? 7 : 6;
  var nRow = is7x6 ? 6 : 5;

  if ($board.length > 0) {
    var startTime = 0;
    var speed = 50 / 1000;
    var slotSize = $board.width() / nCol;
    var pointerOffset = [slotSize/2, slotSize/2 + 15];
    var currentSlot = [-1, -1];
    var $slots = {};
    var currentPath = [];
    $('div.slot:not(:hidden)').each(function() {
      var $this = $(this);
      var i = $this.data('row') * nCol + $this.data('col');
      $slots[i] = $this;
      $this.data('original-type', $this.attr('type'));
    });
    $('div.slot').css({'width': slotSize, 'height': slotSize});
    $pointer.css({'width': slotSize+10, 'height': slotSize+10});
    var boardTop = $board.offset().top;

    function initPage() {
    }

    $(window).on('statechangecomplete', function() {
      initPage();
      FastClick.attach(document.body);
    });
    // $(window).trigger('statechangecomplete');

    function slot(p) {
      return $slots[p[0] * nCol + p[1]];
    }

    // event handlers
    $body.on('click', '#reset-btn', function() {
      var $this = $(this);
      $steps.text(0);
      for (var i in $slots) {
        $slots[i].attr('type', $slots[i].data('original-type'));
      }
      $applyBtn.text('Apply');
      currentPath = [];
      $('.result').addClass('hidden');
    });

    $body.on('click', '#apply-btn', function() {
      var $this = $(this);
      $('#path-input').val(JSON.stringify(currentPath));
      $('#apply-form').submit();
    });

    $body.on('click', '#apply-btn-auto', function() {
      var $this = $(this);
      $('#path-input').val('auto');
      $('#apply-form').submit();
    });

    $body.on('touchstart', 'div.slot', function(e) {
      var d = $(this).data();
      currentSlot[0] = d.row;
      currentSlot[1] = d.col;
      slot(currentSlot).addClass('current');
      $pointer.attr('type', slot(currentSlot).attr('type')).removeClass('hidden');
      var t = e.originalEvent.changedTouches[0];
      var x = parseInt(t.clientX);
      var y = parseInt(t.clientY);
      $pointer.offset({'top': y-pointerOffset[1], 'left': x-pointerOffset[0]});
      startTime = new Date;
      currentPath.push(currentSlot);
    });

    $body.on('touchend', 'div.board', function(e) {
      slot(currentSlot).removeClass('current');
      $pointer.addClass('hidden');
      var nsteps = $steps.text() * 1;
      var t = Math.round(speed * nsteps * 10) / 10;
      var duration = Math.round((new Date - startTime) / 100) / 10;
      // $applyBtn.text('Apply - ' + t + 's (' + duration + 's)');
      $('.result').removeClass('hidden');
      // todo: count combo and colors
      $('.slot.real').removeClass('visited');
      var dir = [[-1, 0], [1, 0], [0, 1], [0, -1]];
      var combos = [];
      while(1) {
        var c = $('.slot.real:not(.visited):first');
        if (c.length === 0) { break; }
        var type = c.attr('type');
        var pending = [c];
        var count = 0;
        while(pending.length > 0) {
          var n = pending.pop();
          count += 1;
          n.addClass('visited');
          var p = [n.data('row'), n.data('col')];
          dir.forEach(function(d) {
            var nn = [p[0] + d[0], p[1] + d[1]];
            if (nn[0] >= 0 && nn[0] < nRow && nn[1] >= 0 && nn[1] < nCol) {
              var $nn = slot(nn);
              if (!$nn.hasClass('visited') && $nn.attr('type') === type) {
                pending.push($nn);
              }
            }
          });
        }
        if (count >= 3) { combos.push(type); }
      }
      $('#f_combo').text(combos.length);
      $('#f_color').text($.unique(combos).length);
    });

    $body.on('touchmove', 'div.board', function(e) {
      var t = e.originalEvent.changedTouches[0];
      var x = parseInt(t.clientX);
      var y = parseInt(t.clientY);
      $pointer.offset({'top': y-pointerOffset[1], 'left': x-pointerOffset[0]});
      y -= boardTop;
      var p = [Math.floor(y / slotSize), Math.floor(x / slotSize)];
      if (p[0] < 0) p[0] = 0;
      if (p[0] > nRow - 1) p[0] = nRow - 1;
      if (p[1] < 0) p[1] = 0;
      if (p[1] > nCol - 1) p[1] = nCol - 1;
      if (p[0] != currentSlot[0] || p[1] != currentSlot[1]) {
        slot(currentSlot).removeClass('current');
        var s = slot(currentSlot);
        var d = slot(p);
        var t = s.attr('type');
        s.attr('type', d.attr('type'));
        d.attr('type', t);
        currentSlot = p;
        currentPath.push(currentSlot);
        slot(currentSlot).addClass('current');
        $steps.text($steps.text() * 1 + 1);
      }
    });
  }

  $body.on('click', 'a.btn-submit', function() {
    $(this).parents('form').submit();
  });

  $('[data-auto-click-in]').each(function() {
    var $this = $(this);
    var s = $this.data('auto-click-in') * 1;
    var timer = setInterval(function() {
      s -= 1;
      $('#sec').text(s);
      if (s === 0) {
        clearInterval(timer);
        console.log('click now');
        $this.click();
      }
    }, 1000);
  });

});
