
# Points to RVM gemset (in deploying server)
require 'capistrano'

# puma
%w[
  thin
  sudo_put_file
].each { |f|
  require File.dirname(__FILE__) + "/../lib/capistrano/#{f}"
}

set :use_sudo, false


# setup and start sidekiq
def start_sidekiq
  require 'sidekiq/capistrano'

  set :sidekiq_role, :app
  set :sidekiq_cmd, "env STAGE=#{project} bundle exec sidekiq -r ./config/boot.rb"
  set :sidekiq_timeout, 10
  set :sidekiq_pid, "#{current_path}/tmp/pids/sidekiq.pid"
  set :sidekiq_processes, 1
end



# Pick-up the environment to deploy to
#set :default_stage, 'lnote'  # force deployer to specify stage
set :stages, Dir.glob('config/deploy/*').map { |n| File.basename(n).chomp(File.extname(n)) }
require 'capistrano/ext/multistage'

# Bundler Configurations
require 'bundler/capistrano'
set :bundle_without, [:development, :test]

# Points to Subversion Repository
set :repository,  "http://svn.birdsage.com/loper/trunk"
set :scm, :subversion
set :scm_username, 'loper'
set :scm_password, 'deployonly'
set :deploy_via, :export  # use svn export instead of svn checkout

# rvm setting, use rvm-capistrano gem to use ruby-1.9.3-p327 on production server
require 'rvm/capistrano'
set :rvm_ruby_string, '1.9.3-p327'

# Number of releases to be kept
set :keep_releases, 2
after "deploy:update", "deploy:cleanup"
after "deploy:update_code", :copy_config_file
after :copy_config_file, :setup_env
after :setup_env, :setup_rc_local
after :resetdb, :"deploy:restart"

# main flow custom tasks
task :copy_config_file do
  if no_config # config file in svn
    run "cp #{release_path}/config/deploy/config/production/#{stage}.yml #{release_path}/config/config.yml"
  else # config file in server
    run "cp #{shared_path}/config.yml #{release_path}/config/config.yml"
  end
end

# move the deployment.rb
task :setup_env do
  run "cd #{release_path} ; bundle exec rake setup_env[#{project}]"
end

# utilities ============================================================================
task :resetdb do
  run "cd #{current_path} ; STAGE=#{project} RACK_ENV=production bundle exec rake resetdb --trace"
end

task :thin_log do
  run "cd #{current_path} ; tail -n10 log/thin.0.log"
end

task :enable_robots_txt, :roles => :app do
  run "cd #{current_path} ; echo 'User-agent: *' > public/robots.txt ; echo 'Disallow: /' >> public/robots.txt"
end

task :disable_robots_txt, :roles => :app do
  run "cd #{current_path} ; rm public/robots.txt"
end

task :flush_redis, :roles => :app do
  run "redis-cli flushdb"
end

task :setup_rc_local, :roles => :app do
  server = stage.to_s.split('_').last
  # /etc/rc.local
  # script to ensure thin processes and sidekiq client live
  rclocal = <<-RC
#!/bin/sh -ex

export RACK_ENV=production

server="#{server}"

for project in `ls -1 /opt/sites` ; do
  dir="/opt/sites/$project"
  if [ ! -f $dir/disabled ]; then
    if [ -f $dir/shared/pids/thin.0.pid ]; then
      thin_pid=$( cat $dir/shared/pids/thin.0.pid )
    else
      thin_pid=999999
    fi
    if ! ps -p $thin_pid > /dev/null ; then
      echo "Starting thin for $project"
      cd $dir/current && su --c -l ubuntu "RACK_ENV=production STAGE=$project bundle exec thin -C config/thin/${project}_${server}.yml start"
    fi

    if [ -f $dir/current/log/sidekiq.log ]; then
      if [ -f $dir/shared/pids/sidekiq.pid ]; then
        sidekiq_pid=$( cat $dir/shared/pids/sidekiq.pid )
      else
        sidekiq_pid=999999
      fi
      if ! ps -p $sidekiq_pid > /dev/null ; then
        echo "Starting sidekiq for $project"
        cd $dir/current && su --c -l ubuntu "RACK_ENV=production STAGE=$project nohup bundle exec sidekiq -r ./config/boot.rb -e production -C $dir/current/config/sidekiq.yml -i 0 -P $dir/shared/pids/sidekiq.pid >> $dir/current/log/sidekiq.log 2>&1 &"
      fi
    fi
  fi
done

exit 0
  RC
  put_sudo rclocal, "/etc/rc.local", "755"

  # add cronjob to execute /etc/rc.local every 5 minutes
  run %Q[#{sudo} bash -c "grep -q 'rc.local' /etc/crontab || echo '*/5 *   * * *   root    /etc/rc.local' >> /etc/crontab"]
end


# supported
# deploy:restart
# sidekiq:restart
