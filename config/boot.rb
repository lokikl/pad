#!/usr/bin/env ruby

BUILD="$Rev: 4571 $"[/\d+/].to_i

$stage = ENV["STAGE"]
if $stage.nil?
  puts "!! STAGE not given, chose shopadmin by default"
  $stage = "shopadmin"
end

Encoding.default_external = "UTF-8"

require 'fileutils'
FileUtils.mkdir_p 'log'

# add all ruby scripts to $LOAD_PATH
lib_path = File.expand_path(File.dirname(__FILE__) + '/..')
$: << lib_path unless $:.include? lib_path

# require tool to measure load time
require 'app/lib/marktime'
timer = Marktime.new

timer.mark "Load basic gems and global variables" do
  ENV["RACK_ENV"] ||= "development"
  $production = ENV["RACK_ENV"] == 'production'
  $load_app ||= false
  raise "$stage is missing" if $stage.nil?

  if $stage == 'aa'
    $loper = true
    $modules = [
      {name:'Dashboard', code:'dashboard', icon:'th'},
      {name:'Amicoamore', code:'amicoamore', icon:'star-empty'},
      {name:'Admin', code:'admin', icon:'user'},
      {name:'Products', code:'products', icon:'shopping-cart'},
      {name:'Members', code:'members', icon:'user'},
      {name:'Orders', code:'orders', icon:'briefcase'},
    ]
  end
  require "bundler/setup"
  require 'singleton'
  require 'yajl'
  require 'json'
  require 'yaml'
end

# deprecated
timer.mark "Load app config" do
  begin
    conf = File.read "config/config.yml" rescue nil
    conf ||= File.read "config/deploy/config/#$stage.yml" rescue nil
    if conf.nil?
      CONFIG = {}
    else
      conf = YAML.load conf
      CONFIG = conf[ENV['RACK_ENV']] ?
        conf[ENV['RACK_ENV']] :
        conf
    end
  rescue
    CONFIG = {}
  end
  if $load_app
    require 'app/main'
  end
end

if endpoint = CONFIG[:comet]
  timer.mark "Connect to Comet #{endpoint}" do
    require 'app/lib/comet'
    $comet = Comet.new endpoint
  end
end

timer.mark "Load Sidekiq Workers" do
  require 'sidekiq'
  require 'sidekiq-status'

  Sidekiq.configure_client do |config|
    config.client_middleware do |chain|
      chain.add Sidekiq::Status::ClientMiddleware
    end
  end

  Sidekiq.configure_server do |config|
    config.server_middleware do |chain|
      chain.add Sidekiq::Status::ServerMiddleware, expiration: 30 * 60
    end
  end

  Dir.glob("app/workers/#$stage/*.rb") { |worker|
    require worker
  }
end

if CONFIG[:db]
  timer.mark "Load database schema" do
    require 'lib/datamapper/time_zone_correction'
    require 'db/schema_base'
    require 'lib/datamapper/array'
    require "db/schema/#$stage"
    Dir["app/models/#$stage/*"].each { |f| require f }

    if $modules
      require 'app/models/loper/admin.rb'
      mcodes = $modules.map { |m| m[:code] }
      Dir["app/models/loper/*.rb"].each { |f|
        mcode = File.basename(f, ".rb")
        require f if mcodes.include? mcode
      }
    end
    require 'db/setup'
    if $load_app
      DataMapper.finalize.auto_upgrade!
      Models.ensure_schema if Models.respond_to? :ensure_schema
    end
  end
end

if $load_app
  app = self.class.const_get $stage.camelize
  app.after_boot if app.respond_to? :after_boot

  Log.inf "Started Loper at #{Time.now} in #{timer.total.round(4)}s"
  puts "Started Loper in #{timer.total.round(4)}s"
end
