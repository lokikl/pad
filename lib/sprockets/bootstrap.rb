
require 'sprockets'
require 'sass'
require 'sprockets-sass'
require_relative 'preprocessor/html_template.rb'

def setup_sprockets compress=false
  if compress
    require 'uglifier'
    require 'yui/compressor'
  end

  Sprockets::Environment.new { |s|
    s.append_path 'app/js'
    s.append_path 'app/css'
    s.append_path 'app/views'

    s.register_engine '.html', HTMLTemplate
    if compress
      s.css_compressor = YUI::CssCompressor.new
      # quote all object literals to make jquery works on ie8
      s.js_compressor = Uglifier.new(
        :output => {
          :ascii_only => false,       # Escape non-ASCII characters
          :comments => :none,         # Preserve comments (:all, :jsdoc, :copyright, :none)
          :inline_script => false,    # Escape occurrences of </script in strings
          :quote_keys => true,        # Quote keys in object literals
          :max_line_len => 32 * 1024, # Maximum line length in minified code
          :bracketize => false,       # Bracketize if, for, do, while or with statements, even if their body is a single statement
          :semicolons => true,        # Separate statements with semicolons
          :preserve_line => false,    # Preserve line numbers in outputs
          :beautify => false,         # Beautify output
          :indent_level => 4,         # Indent level in spaces
          :indent_start => 0,         # Starting indent level
          :space_colon => false,      # Insert space before colons (only with beautifier)
          :width => 80                # Specify line width when beautifier is used (only with beautifier)
        },
        :mangle => {
          :except => ["$super"]       # Argument names to be excluded from mangling
        },                            # Mangle variable and function names, set to false to skip mangling
        :compress => {
          :sequences => true,         # Allow statements to be joined by commas
          :properties => true,        # Rewrite property access using the dot notation
          :dead_code => true,         # Remove unreachable code
          :drop_debugger => true,     # Remove debugger; statements
          :unsafe => false,           # Apply "unsafe" transformations
          :conditionals => true,      # Optimize for if-s and conditional expressions
          :comparisons => true,       # Apply binary node optimizations for comparisons
          :evaluate => true,          # Attempt to evaluate constant expressions
          :booleans => true,          # Various optimizations to boolean contexts
          :loops => true,             # Optimize loops when condition can be statically determined
          :unused => true,            # Drop unreferenced functions and variables
          :hoist_funs => true,        # Hoist function declarations
          :hoist_vars => false,       # Hoist var declarations
          :if_return => true,         # Optimizations for if/return and if/continue
          :join_vars => true,         # Join consecutive var statements
          :cascade => true            # Cascade sequences
        },                            # Apply transformations to code, set to false to skip
        :define => {},                # Define values for symbol replacement
        :enclose => false,            # Enclose in output function wrapper, define replacements as key-value pairs
        :source_filename => nil,      # The filename of the input file
        :source_root => nil,          # The URL of the directory which contains :source_filename
        :output_filename => nil,      # The filename or URL where the minified output can be found
        :input_source_map => nil,     # The contents of the source map describing the input
        :screw_ie8 => false           # Generate safe code for IE8
      )
    end
  }
end
