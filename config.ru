$load_app = true
$compile_asset = true
require File.dirname(__FILE__) + '/config/boot.rb'

unless $production
  map '/assets' do
    require './lib/sprockets/bootstrap'
    require 'sprockets-sass'
    Sprockets::Sass.options[:debug_info] = true
    Sprockets::Sass.options[:line_comments] = true
    Sprockets::Sass.options[:sourcemap] = false
    sprockets = setup_sprockets

    run sprockets
  end
end

map '/' do
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8

  session_conf = {
    secret: "SecrEtIsASeCreT",
    expire_after: 1.week,
  }

  unless $production
    #require 'rack-livereload'
    #use Rack::LiveReload
    session_conf[:domain] = "localhost.com"
  else
    session_conf[:domain] = CONFIG[:frontend] if CONFIG[:frontend]
  end

  # puts "session_conf: #{session_conf.inspect}"
  use Rack::Session::Cookie, session_conf

  klass = eval $stage.capitalize
  run klass
end

if $loper
  map '/loper' do
    #Encoding.default_external = Encoding::UTF_8
    #Encoding.default_internal = Encoding::UTF_8

    use Rack::Session::Cookie
    run Loper
  end
end

if CONFIG[:sidekiq]
  map '/sidekiq' do
    require 'sidekiq'
    require 'sidekiq/web'

    Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      username == 'admin' && password == 'admin'
    end

    run Sidekiq::Web
  end
end
